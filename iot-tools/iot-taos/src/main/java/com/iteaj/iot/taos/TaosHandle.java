package com.iteaj.iot.taos;

import com.iteaj.iot.Protocol;
import com.iteaj.iot.ProtocolHandle;

public interface TaosHandle<T extends Protocol> extends ProtocolHandle<T>, IotTaos {

    /**
     * @see TaosEntity
     * @param protocol
     * @return 返回的对象必须是 {@link TaosEntity}的子类
     */
    @Override
    Object handle(T protocol);
}
