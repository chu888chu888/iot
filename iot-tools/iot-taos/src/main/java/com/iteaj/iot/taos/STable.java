package com.iteaj.iot.taos;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用来声明超级表名, 可以自动创建数据表
 * @see TaosEntity 使用在其子类上 不注解将不使用超级表作为模板
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface STable {

    String value();
}
