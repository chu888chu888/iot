package com.iteaj.iot.taos;

/**
 * taos操作实体
 */
public interface TaosEntity {

    /**
     * 获取要操作的数据表
     * 注：不能使用超级表(stable)
     * @return
     */
    String getTable();
}
