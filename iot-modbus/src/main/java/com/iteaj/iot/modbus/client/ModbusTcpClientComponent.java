package com.iteaj.iot.modbus.client;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.MultiClientManager;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.component.TcpClientComponent;
import com.iteaj.iot.modbus.client.message.ModbusTcpClientMessage;

public class ModbusTcpClientComponent<M extends ModbusTcpClientMessage> extends TcpClientComponent<M> {

    private static final String DESC = "基于Modbus Tcp协议的Iot客户端实现";

    public ModbusTcpClientComponent(ClientConnectProperties config) {
        super(config);
    }

    public ModbusTcpClientComponent(ClientConnectProperties config, MultiClientManager clientManager) {
        super(config, clientManager);
    }

    @Override
    public TcpSocketClient createNewClient(ClientConnectProperties config) {
        return new ModbusTcpClient(this, config);
    }

    @Override
    public String getName() {
        return "ModbusTcpClient";
    }

    @Override
    public String getDesc() {
        return DESC;
    }

    @Override
    public AbstractProtocol getProtocol(M message) {
        return remove(message.getHead().getMessageId());
    }

}
