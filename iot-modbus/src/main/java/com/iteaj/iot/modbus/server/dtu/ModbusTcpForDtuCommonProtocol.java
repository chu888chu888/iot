package com.iteaj.iot.modbus.server.dtu;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.modbus.*;
import com.iteaj.iot.modbus.consts.ModbusCode;
import com.iteaj.iot.modbus.consts.ModbusCoilStatus;
import com.iteaj.iot.server.protocol.ServerInitiativeProtocol;

import java.io.IOException;

/**
 * modbus tcp的服务端通用操作协议, 使用Dtu设备连网
 */
public class ModbusTcpForDtuCommonProtocol extends ServerInitiativeProtocol<ModbusTcpForDtuMessage> {

    /**
     * 寄存器开始地址
     */
    private int start;

    /**
     * modbus 功能码
     */
    private ModbusCode code;

    private Payload payload;

    protected ModbusTcpForDtuCommonProtocol(ModbusCode code, ModbusTcpForDtuMessage message) {
        this.code = code;
        this.setRequestMessage(message);
    }

    protected ModbusTcpForDtuCommonProtocol(int start, ModbusCode code, ModbusTcpForDtuMessage message) {
        this.start = start;
        this.code = code;
        this.setRequestMessage(message);
    }

    @Override
    protected ModbusTcpForDtuMessage doBuildRequestMessage() throws IOException {
        return this.requestMessage;
    }

    @Override
    protected void doBuildResponseMessage(ModbusTcpForDtuMessage message) {
        byte[] content = message.getBody().getContent();

        switch (this.code) {
            case Read01:
            case Read02:
                this.payload = new RealCoilPayload(content); break;
            case Read03:
            case Read04:
                this.payload = new ReadPayload(content, this.start); break;
            default:
                this.payload = WritePayload.getInstance();
        }
    }

    public Payload getPayload() {
        return payload;
    }

    /**
     * 构建Modbus读线圈协议
     * @see ModbusCode#Read01
     * @param equipCode 要操作的设备的设备编号
     * @param device 从机的设备地址
     * @param start 从哪个寄存器开始读
     * @param bitNum 读多少位(一个字节8位)
     * @return
     */
    public static ModbusTcpForDtuCommonProtocol buildRead01(String equipCode, int device, int start, int bitNum) {
        ModbusTcpForDtuMessage message = ModbusTcpMessageBuilder.buildRead01Message(new ModbusTcpForDtuMessage(equipCode), device, start, bitNum);
        return new ModbusTcpForDtuCommonProtocol(ModbusCode.Read01, message);
    }

    /**
     * 构建Modbus读线圈协议
     * @see ModbusCode#Read02
     * @param equipCode 要操作的设备的设备编号
     * @param device 从机的设备地址
     * @param start 从哪个寄存器开始读
     * @param bitNum 读多少位(一个字节8位)
     * @return
     */
    public static ModbusTcpForDtuCommonProtocol buildRead02(String equipCode, int device, int start, int bitNum) {
        ModbusTcpForDtuMessage message = ModbusTcpMessageBuilder.buildRead02Message(new ModbusTcpForDtuMessage(equipCode), device, start, bitNum);
        return new ModbusTcpForDtuCommonProtocol(ModbusCode.Read02, message);
    }

    /**
     * 构建Modbus读保持寄存器报文
     * @param equipCode 要操作的设备的设备编号
     * @param device 从机的设备地址
     * @param start 从哪个寄存器开始读
     * @param num 读几个寄存器
     * @return
     */
    public static ModbusTcpForDtuCommonProtocol buildRead03(String equipCode, int device, int start, int num) {
        ModbusTcpForDtuMessage message = ModbusTcpMessageBuilder.buildRead03Message(new ModbusTcpForDtuMessage(equipCode), device, start, num);
        return new ModbusTcpForDtuCommonProtocol(start, ModbusCode.Read03, message);
    }

    /**
     * 构建Modbus读输入寄存器协议
     * @param equipCode 要操作的设备的设备编号
     * @param device 从机的设备地址 (1-255)
     * @param start 从哪个寄存器开始读 (1-65535)
     * @param num 读几个寄存器(1-2000)
     * @return
     */
    public static ModbusTcpForDtuCommonProtocol buildRead04(String equipCode, int device, int start, int num) {
        ModbusTcpForDtuMessage message = ModbusTcpMessageBuilder.buildRead04Message(new ModbusTcpForDtuMessage(equipCode), device, start, num);
        return new ModbusTcpForDtuCommonProtocol(start, ModbusCode.Read04, message);
    }

    /**
     * 构建Modbus写单个线圈报文
     * @param equipCode 要操作的设备的设备编号
     * @param device 访问的设备
     * @param start 从哪个寄存器开始写
     * @param status 写内容
     * @return
     */
    public static ModbusTcpForDtuCommonProtocol buildWrite05(String equipCode, int device, int start, ModbusCoilStatus status) {
        ModbusTcpForDtuMessage message = ModbusTcpMessageBuilder.buildWrite05Message(new ModbusTcpForDtuMessage(equipCode), device, start, status);
        return new ModbusTcpForDtuCommonProtocol(ModbusCode.Write05, message);
    }

    /**
     * 构建Modbus写单个寄存器报文
     * @param equipCode 要操作的设备的设备编号
     * @param device 从机的设备地址 (1-255)
     * @param start 从哪个寄存器开始写 (1-65535)
     * @param write 写内容
     * @return
     */
    public static ModbusTcpForDtuCommonProtocol buildWrite06(String equipCode, int device, int start, byte[] write) {
        ModbusTcpForDtuMessage message = ModbusTcpMessageBuilder.buildWrite06Message(new ModbusTcpForDtuMessage(equipCode), device, start, write);
        return new ModbusTcpForDtuCommonProtocol(ModbusCode.Write06, message);
    }

    /**
     * 构建Modbus写多个寄存器报文
     * @param equipCode 要操作的设备的设备编号
     * @param device 从机的设备地址 (1-255)
     * @param start 从哪个寄存器开始写
     * @param write 写到设备的内容 1Byte = 8bit = 8个线圈
     * @return
     */
    public static ModbusTcpForDtuCommonProtocol buildWrite0F(String equipCode, int device, int start, byte[] write) {
        ModbusTcpForDtuMessage message = ModbusTcpMessageBuilder.buildWrite0FMessage(new ModbusTcpForDtuMessage(equipCode), device, start, write);
        return new ModbusTcpForDtuCommonProtocol(ModbusCode.Write0F, message);
    }

    /**
     * 构建Modbus写多个寄存器报文
     * @param equipCode 要操作的设备的设备编号
     * @param device 从机的设备地址 (1-255)
     * @param start 从哪个寄存器开始写
     * @param num 写几个寄存器
     * @param write 写到设备的内容
     * @return
     */
    public static ModbusTcpForDtuCommonProtocol buildWrite10(String equipCode, int device, int start, int num, byte[] write) {
        ModbusTcpForDtuMessage message = ModbusTcpMessageBuilder.buildWrite10Message(new ModbusTcpForDtuMessage(equipCode), device, start, num, write);
        return new ModbusTcpForDtuCommonProtocol(ModbusCode.Write10, message);
    }

    @Override
    public ProtocolType protocolType() {
        return this.code;
    }
}
