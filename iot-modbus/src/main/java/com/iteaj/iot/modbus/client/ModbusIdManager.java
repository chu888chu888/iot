package com.iteaj.iot.modbus.client;

public interface ModbusIdManager {

    /**
     * 下一个id
     * @return
     */
    short nextId();
}
