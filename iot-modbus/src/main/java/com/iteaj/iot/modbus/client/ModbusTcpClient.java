package com.iteaj.iot.modbus.client;

import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.client.*;
import com.iteaj.iot.client.protocol.ClientSocketProtocol;
import com.iteaj.iot.codec.IotMessageDecoder;
import com.iteaj.iot.codec.adapter.LengthFieldBasedFrameMessageDecoderAdapter;
import com.iteaj.iot.modbus.ModbusTcpHeader;
import com.iteaj.iot.modbus.ModbusTcpMessageBuilder;
import com.iteaj.iot.modbus.client.message.ModbusTcpClientMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;

/**
 *
 * Modbus Tcp协议实现的客户端
 * @see ModbusTcpClientMessage
 */
public class ModbusTcpClient extends TcpSocketClient {

    private int maxFrameLength;

    /**
     * 默认报文最大长度 1024字节
     * @param clientComponent
     * @param config
     */
    public ModbusTcpClient(ClientComponent clientComponent, ClientConnectProperties config) {
        this(clientComponent, config, 512);
    }

    public ModbusTcpClient(ClientComponent clientComponent, ClientConnectProperties config, int maxFrameLength) {
        super(clientComponent, config);
        this.maxFrameLength = maxFrameLength;
    }

    @Override
    protected IotMessageDecoder createProtocolDecoder() {
        return new LengthFieldBasedFrameMessageDecoderAdapter(this.maxFrameLength, 4, 2, 0, 0, true) {

            @Override
            public ModbusTcpClientMessage createMessage(byte[] message) {
                SocketMessage socketMessage = super.createMessage(message);
                if(socketMessage instanceof ModbusTcpClientMessage) {
                    // 使用Channel id作为设备编号
                    return ((ModbusTcpClientMessage) socketMessage).setEquipCode(getChannel().id().asShortText());
                } else {
                    throw new ClientProtocolException("报文类型必须是["+ModbusTcpClientMessage.class.getSimpleName()+"]或其子类");
                }
            }
        };
    }

    @Override
    public ChannelFuture writeAndFlush(ClientSocketProtocol clientProtocol) {
        ClientMessage clientMessage = clientProtocol.requestMessage();
        if(clientMessage instanceof ModbusTcpClientMessage) {
            short nextId = ModbusTcpMessageBuilder.getNextId(getChannel());

            // 使用Channel id作为设备编号
            String equipCode = getChannel().id().asShortText();
            ((ModbusTcpClientMessage) clientMessage).setEquipCode(equipCode);
            ModbusTcpHeader head = (ModbusTcpHeader) clientMessage.getHead();
            head.setEquipCode(equipCode);
            ModbusTcpMessageBuilder.buildMessageHeadByNextId(nextId, head);
        }

        return super.writeAndFlush(clientProtocol);
    }
}
