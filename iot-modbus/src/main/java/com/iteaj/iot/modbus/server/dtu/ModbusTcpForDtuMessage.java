package com.iteaj.iot.modbus.server.dtu;

import com.iteaj.iot.modbus.ModbusTcpBody;
import com.iteaj.iot.modbus.ModbusTcpHeader;
import com.iteaj.iot.modbus.server.message.ModbusTcpServerMessage;
import com.iteaj.iot.server.dtu.DtuCommonProtocolType;
import com.iteaj.iot.server.dtu.message.DtuMessage;

public class ModbusTcpForDtuMessage extends ModbusTcpServerMessage implements DtuMessage {

    public ModbusTcpForDtuMessage(byte[] message) {
        super(message);
    }

    public ModbusTcpForDtuMessage(String equipCode) {
        super(equipCode);
    }

    public ModbusTcpForDtuMessage(ModbusTcpHeader head) {
        super(head);
    }

    public ModbusTcpForDtuMessage(ModbusTcpHeader head, ModbusTcpBody body) {
        super(head, body);
    }

    @Override
    public MessageHead buildFirstHead() {
        return ModbusTcpHeader.buildRequestHeader(this.getEquipCode(), this.getEquipCode(), DtuCommonProtocolType.DEVICE_SN);
    }
}
