package com.iteaj.iot.modbus.client;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.modbus.*;
import com.iteaj.iot.modbus.client.message.ModbusTcpClientMessage;
import com.iteaj.iot.modbus.consts.ModbusCode;
import com.iteaj.iot.modbus.consts.ModbusCoilStatus;

/**
 * Modbus tcp 客户端通用协议
 */
public class ModbusTcpClientCommonProtocol extends ClientInitiativeProtocol<ModbusTcpClientMessage> {

    private int start;
    private ModbusCode code;
    private Payload payload;

    protected ModbusTcpClientCommonProtocol(ModbusCode code, ModbusTcpClientMessage message) {
        this.code = code;
        this.requestMessage = message;
    }

    public ModbusTcpClientCommonProtocol(int start, ModbusCode code, ModbusTcpClientMessage message) {
        this.start = start;
        this.code = code;
        this.requestMessage = message;
    }

    @Override
    protected ModbusTcpClientMessage doBuildRequestMessage() {
        return this.requestMessage;
    }

    @Override
    public void doBuildResponseMessage(ModbusTcpClientMessage message) {
        byte[] content = message.getBody().getContent();

        switch (this.code) {
            case Read01:
            case Read02:
                this.payload = new RealCoilPayload(content); break;
            case Read03:
            case Read04:
                this.payload = new ReadPayload(content, this.start); break;
            default:
                this.payload = WritePayload.getInstance();
        }
    }

    public Payload getPayload() {
        return payload;
    }

    /**
     * 构建Modbus读线圈协议
     * @see ModbusCode#Read01
     * @param device 从机的设备地址
     * @param start 从哪个寄存器开始读
     * @param bitNum 读多少位(一个字节8位)
     * @return
     */
    public static ModbusTcpClientCommonProtocol buildRead01(int device, int start, int bitNum) {
        ModbusTcpClientMessage message = ModbusTcpMessageBuilder.buildRead01Message(new ModbusTcpClientMessage(), device, start, bitNum);
        return new ModbusTcpClientCommonProtocol(ModbusCode.Read01, message);
    }

    /**
     * 构建Modbus读线圈协议
     * @see ModbusCode#Read02
     * @param device 从机的设备地址
     * @param start 从哪个寄存器开始读
     * @param bitNum 读多少位(一个字节8位)
     * @return
     */
    public static ModbusTcpClientCommonProtocol buildRead02(int device, int start, int bitNum) {
        ModbusTcpClientMessage message = ModbusTcpMessageBuilder.buildRead02Message(new ModbusTcpClientMessage(), device, start, bitNum);
        return new ModbusTcpClientCommonProtocol(ModbusCode.Read02, message);
    }

    /**
     * 构建Modbus读保持寄存器报文
     * @param device 从机的设备地址
     * @param start 从哪个寄存器开始读
     * @param num 读几个寄存器
     * @return
     */
    public static ModbusTcpClientCommonProtocol buildRead03(int device, int start, int num) {
        ModbusTcpClientMessage message = ModbusTcpMessageBuilder.buildRead03Message(new ModbusTcpClientMessage(), device, start, num);
        return new ModbusTcpClientCommonProtocol(start, ModbusCode.Read03, message);
    }

    /**
     * 构建Modbus读输入寄存器协议
     * @param device 从机的设备地址 (1-255)
     * @param start 从哪个寄存器开始读 (1-65535)
     * @param num 读几个寄存器(1-2000)
     * @return
     */
    public static ModbusTcpClientCommonProtocol buildRead04(int device, int start, int num) {
        ModbusTcpClientMessage message = ModbusTcpMessageBuilder.buildRead04Message(new ModbusTcpClientMessage(), device, start, num);
        return new ModbusTcpClientCommonProtocol(start, ModbusCode.Read04, message);
    }

    /**
     * 构建Modbus写单个线圈报文
     * @param device 访问的设备
     * @param start 从哪个寄存器开始写
     * @param status 写内容
     * @return
     */
    public static ModbusTcpClientCommonProtocol buildWrite05(int device, int start, ModbusCoilStatus status) {
        ModbusTcpClientMessage message = ModbusTcpMessageBuilder.buildWrite05Message(new ModbusTcpClientMessage(), device, start, status);
        return new ModbusTcpClientCommonProtocol(ModbusCode.Write05, message);
    }

    /**
     * 构建Modbus写单个寄存器报文
     * @param device 从机的设备地址 (1-255)
     * @param start 从哪个寄存器开始写 (1-65535)
     * @param write 写内容
     * @return
     */
    public static ModbusTcpClientCommonProtocol buildWrite06(int device, int start, byte[] write) {
        ModbusTcpClientMessage message = ModbusTcpMessageBuilder.buildWrite06Message(new ModbusTcpClientMessage(), device, start, write);
        return new ModbusTcpClientCommonProtocol(ModbusCode.Write06, message);
    }

    /**
     * 构建Modbus写多个寄存器报文
     * @param device 从机的设备地址 (1-255)
     * @param start 从哪个寄存器开始写
     * @param write 写到设备的内容
     * @return
     */
    public static ModbusTcpClientCommonProtocol buildWrite0F(int device, int start, byte[] write) {
        ModbusTcpClientMessage message = ModbusTcpMessageBuilder.buildWrite0FMessage(new ModbusTcpClientMessage(), device, start, write);
        return new ModbusTcpClientCommonProtocol(ModbusCode.Write0F, message);
    }

    /**
     * 构建Modbus写多个寄存器报文
     * @param device 从机的设备地址 (1-255)
     * @param start 从哪个寄存器开始写
     * @param num 写几个寄存器
     * @param write 写到设备的内容
     * @return
     */
    public static ModbusTcpClientCommonProtocol buildWrite10(int device, int start, int num, byte[] write) {
        ModbusTcpClientMessage message = ModbusTcpMessageBuilder.buildWrite10Message(new ModbusTcpClientMessage(), device, start, num, write);
        return new ModbusTcpClientCommonProtocol(ModbusCode.Write10, message);
    }

    @Override
    public ProtocolType protocolType() {
        return this.code;
    }
}
