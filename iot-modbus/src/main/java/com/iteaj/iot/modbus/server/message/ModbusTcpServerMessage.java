package com.iteaj.iot.modbus.server.message;

import com.iteaj.iot.modbus.ModbusTcpBody;
import com.iteaj.iot.modbus.ModbusTcpHeader;
import com.iteaj.iot.modbus.ModbusTcpMessage;
import com.iteaj.iot.server.ServerMessage;

public abstract class ModbusTcpServerMessage extends ServerMessage implements ModbusTcpMessage {

    private String equipCode;

    public ModbusTcpServerMessage(byte[] message) {
        super(message);
    }

    public ModbusTcpServerMessage(String equipCode) {
        super(EMPTY);
        this.equipCode = equipCode;
    }

    protected ModbusTcpServerMessage(ModbusTcpHeader head) {
        super(head);
    }

    protected ModbusTcpServerMessage(ModbusTcpHeader head, ModbusTcpBody body) {
        super(head, body);
    }

    @Override
    protected MessageHead doBuild(byte[] message) {
        this.messageBody = ModbusTcpBody.buildResponseBody(message);

        ModbusTcpHeader modbusTcpHeader = ModbusTcpHeader.buildResponseHeader(message);
        modbusTcpHeader.setEquipCode(this.equipCode);
        return modbusTcpHeader.buildMessageId();
    }

    public String getEquipCode() {
        return equipCode;
    }

    public void setEquipCode(String equipCode) {
        this.equipCode = equipCode;
    }

    @Override
    public ModbusTcpBody getBody() {
        return (ModbusTcpBody) super.getBody();
    }

    @Override
    public ModbusTcpHeader getHead() {
        return (ModbusTcpHeader) super.getHead();
    }
}
