package com.iteaj.iot.modbus.client;

public class SimpleModbusIdManager implements ModbusIdManager {

    private Integer messageId = new Integer(0);

    @Override
    public synchronized short nextId() {
        int andIncrement = ++ messageId;
        if(andIncrement == 0xFFFF) {
            messageId = 0; // 从0开始
        }

        return (short) andIncrement;
    }
}
