package com.iteaj.iot.modbus;

import com.iteaj.iot.utils.ByteUtil;

public class ReadPayload extends Payload{

    private int start;

    public ReadPayload(byte[] payload, int start) {
        super(payload);
        this.start = start;
    }

    public short readShort(int start) {
        validatorStartAddress(start);
        int offset = (start - this.start) * 2;
        return ByteUtil.bytesToShort(this.getPayload(), offset);
    }


    public int readUShort(int start) {
        validatorStartAddress(start);
        int offset = (start - this.start) * 2;
        return ByteUtil.bytesToUShort(this.getPayload(), offset);
    }

    public int readInt(int start) {
        validatorStartAddress(start);
        int offset = (start - this.start) * 2;
        return ByteUtil.bytesToInt(this.getPayload(), offset);
    }

    public long readUInt(int start) {
        validatorStartAddress(start);
        int offset = (start - this.start) * 2;
        return ByteUtil.bytesToUInt(this.getPayload(), offset);
    }

    public long readLong(int start) {
        validatorStartAddress(start);
        int offset = (start - this.start) * 2;
        return ByteUtil.bytesToLong(this.getPayload(), offset);
    }

    @Override
    public float readFloat(int start) {
        validatorStartAddress(start);
        int offset = (start - this.start) * 2;
        return ByteUtil.bytesToFloat(this.getPayload(), offset);
    }

    @Override
    public double readDouble(int start) {
        validatorStartAddress(start);
        int offset = (start - this.start) * 2;
        return ByteUtil.bytesToDouble(this.getPayload(), offset);
    }

    private void validatorStartAddress(int start) {
        if(start < this.start) {
            throw new IllegalArgumentException("读取的寄存器地址不能小于["+this.start+"]");
        }
    }
}
