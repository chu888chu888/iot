package com.iteaj.iot.modbus.server.dtu;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.Protocol;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.modbus.ModbusTcpHeader;
import com.iteaj.iot.modbus.ModbusTcpMessageBuilder;
import com.iteaj.iot.server.dtu.LengthFieldBasedFrameForDtuDecoderComponent;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;

/**
 * 适用于：<hr>
 *     首先使用Dtu连网, dtu必须第一个报文必须上报设备编号
 *     其次dtu连接的设备必须是使用标准的Modbus Tcp协议
 * @param <M>
 */
public class ModbusTcpForDtuDecodeComponent<M extends ModbusTcpForDtuMessage> extends LengthFieldBasedFrameForDtuDecoderComponent<M> {

    public ModbusTcpForDtuDecodeComponent(ConnectProperties connectProperties) {
        this(connectProperties, 512);
    }

    public ModbusTcpForDtuDecodeComponent(ConnectProperties connectProperties, int maxFrameLength) {
        super(connectProperties, maxFrameLength, 4, 2, 0, 0, true);
    }

    @Override
    public String getName() {
        return "ModbusTcpServerForDtu";
    }

    @Override
    public String getDesc() {
        return "使用Dtu连网且设备基于标准Modbus Tcp协议的iot服务端实现";
    }

    @Override
    public AbstractProtocol doGetProtocol(M message) {
        return remove(message.getHead().getMessageId());
    }

    @Override
    public ChannelFuture writeAndFlush(String equipCode, Protocol protocol) {
        Channel channel = getDeviceManager().find(equipCode);
        if(channel != null) {
            ModbusTcpHeader head = (ModbusTcpHeader)protocol.requestMessage().getHead();

            // 设置Modbus的递增值和messageId
            short nextId = ModbusTcpMessageBuilder.getNextId(channel);
            ModbusTcpMessageBuilder.buildMessageHeadByNextId(nextId, head);
        }

        return super.writeAndFlush(equipCode, protocol);
    }

    /**
     * 注：子类必须覆盖此方法, 返回对应的报文<M>
     * @param message
     * @return
     */
    @Override
    public M createMessage(byte[] message) {
        return (M) new ModbusTcpForDtuMessage(message);
    }
}
