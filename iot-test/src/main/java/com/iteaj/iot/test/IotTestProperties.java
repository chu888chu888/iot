package com.iteaj.iot.test;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.mqtt.MqttConnectProperties;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.plc.omron.OmronConnectProperties;
import com.iteaj.iot.plc.siemens.SiemensConnectProperties;
import com.iteaj.iot.plc.siemens.SiemensModel;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "iot.test")
public class IotTestProperties {

    /**
     * 启用客户端测试
     */
    private boolean client;

    /**
     * 启用服务端测试
     */
    private boolean server;

    /**
     * 基于换行符解码组件测试
     */
    private TestConnectConfig line;

    /**
     * 基于固定长度解码器测试
     */
    private TestConnectConfig fixed;

    /**
     * mqtt协议测试
     */
    private TestMqttConnectProperties mqtt;

    /**
     * Modbus Tcp 协议测试
     */
    private TestConnectConfig modbus;

    /**
     * modbus tcp Of Dtu 测试
     */
    private ModbusTcpDtuConnectProperties modbusDtu;

    /**
     * 西门子PLC 协议测试
     */
    private TestSiemensConnectProperties siemens;

    /**
     * 欧姆龙PLC 协议测试
     */
    private TestOmronConnectProperties omron;

    /**
     * 服务端主机地址
     */
    private String host = "127.0.0.1";

    public boolean isClient() {
        return client;
    }

    public void setClient(boolean client) {
        this.client = client;
    }

    public boolean isServer() {
        return server;
    }

    public void setServer(boolean server) {
        this.server = server;
    }

    public TestConnectConfig getLine() {
        return line;
    }

    public void setLine(TestConnectConfig line) {
        this.line = line;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public static class TestConnectConfig extends ClientConnectProperties {

        public TestConnectConfig() { }

        public TestConnectConfig(Integer port) {
            super(port);
        }

        public TestConnectConfig(String host, Integer port) {
            super(host, port);
        }

        /**
         * 测试的客户端数量
         */
        private int num;

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }
    }

    public static class  ModbusTcpDtuConnectProperties extends ConnectProperties {
        /**
         * 是否启用测试
         */
        private boolean start;

        public boolean isStart() {
            return start;
        }

        public void setStart(boolean start) {
            this.start = start;
        }
    }

    public static class TestMqttConnectProperties extends MqttConnectProperties {
        /**
         * 测试的客户端数量
         */
        private int num;

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }
    }

    public static class TestSiemensConnectProperties extends SiemensConnectProperties {

        /**
         * 是否启用测试
         */
        private boolean start;

        public TestSiemensConnectProperties() {
            super("127.0.0.1", SiemensModel.S1200);
        }

        public TestSiemensConnectProperties(String host, SiemensModel model) {
            super(host, model);
        }

        public TestSiemensConnectProperties(String host, Integer port, SiemensModel model) {
            super(host, port, model);
        }

        public boolean isStart() {
            return start;
        }

        public void setStart(boolean start) {
            this.start = start;
        }
    }

    public static class TestOmronConnectProperties extends OmronConnectProperties {

        private boolean start;

        public TestOmronConnectProperties() {
            super("127.0.0.1", 9600);
        }

        public boolean isStart() {
            return start;
        }

        public void setStart(boolean start) {
            this.start = start;
        }
    }

    public TestConnectConfig getFixed() {
        return fixed;
    }

    public void setFixed(TestConnectConfig fixed) {
        this.fixed = fixed;
    }

    public TestMqttConnectProperties getMqtt() {
        return mqtt;
    }

    public void setMqtt(TestMqttConnectProperties mqtt) {
        this.mqtt = mqtt;
    }

    public TestConnectConfig getModbus() {
        return modbus;
    }

    public void setModbus(TestConnectConfig modbus) {
        this.modbus = modbus;
    }

    public TestSiemensConnectProperties getSiemens() {
        return siemens;
    }

    public void setSiemens(TestSiemensConnectProperties siemens) {
        this.siemens = siemens;
    }

    public TestOmronConnectProperties getOmron() {
        return omron;
    }

    public void setOmron(TestOmronConnectProperties omron) {
        this.omron = omron;
    }

    public ModbusTcpDtuConnectProperties getModbusDtu() {
        return modbusDtu;
    }

    public void setModbusDtu(ModbusTcpDtuConnectProperties modbusDtu) {
        this.modbusDtu = modbusDtu;
    }
}
