package com.iteaj.iot.test.modbus;

import com.iteaj.iot.Message;
import com.iteaj.iot.client.ClientProtocolHandle;
import com.iteaj.iot.modbus.ModbusTcpBody;
import com.iteaj.iot.modbus.client.ModbusTcpClientComponent;
import com.iteaj.iot.modbus.consts.ModbusCode;
import com.iteaj.iot.test.TestConst;
import com.iteaj.iot.utils.ByteUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.time.Instant;

public class ModbusTestHandle implements ClientProtocolHandle<ModbusStatusProtocol>, InitializingBean {

    @Autowired
    private ThreadPoolTaskScheduler scheduler;
    @Autowired
    private ModbusTcpClientComponent modbusTcpClientComponent;

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Object handle(ModbusStatusProtocol protocol) {
        return null;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        scheduler.schedule(() -> {
            System.out.println("------------------ modbus客户端测试时使用的服务端模拟工具地址(https://gitee.com/qsaker/QtSwissArmyKnife) -------------");
            new ModbusStatusProtocol(ModbusCode.Write05).request(protocol -> {
                Message.MessageHead head = protocol.requestMessage().getHead();
                ModbusTcpBody body = (ModbusTcpBody)protocol.responseMessage().getBody();
                logger.info(TestConst.LOGGER_PROTOCOL_DESC, modbusTcpClientComponent.getName()
                        , "Write05", head.getEquipCode(), head.getMessageId()
                        , body.isSuccess() ? "通过" : "失败("+body.getErrCode().getDesc()+")");

                return null;
            });

            new ModbusStatusProtocol(ModbusCode.Write06).request(protocol -> {
                Message.MessageHead head = protocol.requestMessage().getHead();
                ModbusTcpBody body = (ModbusTcpBody)protocol.responseMessage().getBody();
                logger.info(TestConst.LOGGER_PROTOCOL_DESC, modbusTcpClientComponent.getName()
                        , "Write06", head.getEquipCode(), head.getMessageId()
                        , body.isSuccess() ? "通过" : "失败("+body.getErrCode().getDesc()+")");

                return null;
            });

            new ModbusStatusProtocol(ModbusCode.Write0F).request(protocol -> {
                Message.MessageHead head = protocol.requestMessage().getHead();
                ModbusTcpBody body = (ModbusTcpBody)protocol.responseMessage().getBody();
                logger.info(TestConst.LOGGER_PROTOCOL_DESC, modbusTcpClientComponent.getName()
                        , "Write0F", head.getEquipCode(), head.getMessageId()
                        , body.isSuccess() ? "通过" : "失败("+body.getErrCode().getDesc()+")");

                return null;
            });

            new ModbusStatusProtocol(ModbusCode.Write10).request(protocol -> {
                Message.MessageHead head = protocol.requestMessage().getHead();
                ModbusTcpBody body = (ModbusTcpBody)protocol.responseMessage().getBody();
                logger.info(TestConst.LOGGER_PROTOCOL_DESC, modbusTcpClientComponent.getName()
                        , "Write10", head.getEquipCode(), head.getMessageId()
                        , body.isSuccess() ? "通过" : "失败("+body.getErrCode().getDesc()+")");

                return null;
            });

            new ModbusStatusProtocol(ModbusCode.Read01).request(protocol -> {
                Message.MessageHead head = protocol.requestMessage().getHead();
                ModbusTcpBody body = (ModbusTcpBody)protocol.responseMessage().getBody();
                System.out.println(ByteUtil.bytesToHex(body.getContent()));
                logger.info(TestConst.LOGGER_PROTOCOL_DESC, modbusTcpClientComponent.getName()
                        , "Read01", head.getEquipCode(), head.getMessageId()
                        , body.isSuccess() ? "通过" : "失败("+body.getErrCode().getDesc()+")");
                return null;
            });
            new ModbusStatusProtocol(ModbusCode.Read02).request(protocol -> {
                Message.MessageHead head = protocol.requestMessage().getHead();
                ModbusTcpBody body = (ModbusTcpBody)protocol.responseMessage().getBody();
                System.out.println(ByteUtil.bytesToHex(body.getContent()));
                logger.info(TestConst.LOGGER_PROTOCOL_DESC, modbusTcpClientComponent.getName()
                        , "Read02", head.getEquipCode(), head.getMessageId()
                        , body.isSuccess() ? "通过" : "失败("+body.getErrCode().getDesc()+")");

                return null;
            });
            new ModbusStatusProtocol(ModbusCode.Read03).request(protocol -> {
                Message.MessageHead head = protocol.requestMessage().getHead();
                ModbusTcpBody body = (ModbusTcpBody)protocol.responseMessage().getBody();
                System.out.println(ByteUtil.bytesToHex(body.getContent()));
                logger.info(TestConst.LOGGER_PROTOCOL_DESC, modbusTcpClientComponent.getName()
                        , "Read03", head.getEquipCode(), head.getMessageId()
                        , body.isSuccess() ? "通过" : "失败("+body.getErrCode().getDesc()+")");

                return null;
            });
            new ModbusStatusProtocol(ModbusCode.Read04).request(protocol -> {
                Message.MessageHead head = protocol.requestMessage().getHead();
                ModbusTcpBody body = (ModbusTcpBody)protocol.responseMessage().getBody();
                System.out.println(ByteUtil.bytesToHex(body.getContent()));
                logger.info(TestConst.LOGGER_PROTOCOL_DESC, modbusTcpClientComponent.getName()
                        , "Read04", head.getEquipCode(), head.getMessageId()
                        , body.isSuccess() ? "通过" : "失败("+body.getErrCode().getDesc()+")");

                return null;
            });

        }, Instant.now().plusSeconds(8));
    }
}
