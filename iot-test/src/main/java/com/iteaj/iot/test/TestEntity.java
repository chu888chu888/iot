package com.iteaj.iot.test;

import com.iteaj.iot.taos.TaosEntity;

import java.util.Date;

public class TestEntity implements TaosEntity {

    private static final String TABLE_PREFIX = "t_";

    private Date ts;

    private String table;

    @Override
    public String getTable() {
        return TABLE_PREFIX + this.table;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public void setTable(String table) {
        this.table = table;
    }
}
