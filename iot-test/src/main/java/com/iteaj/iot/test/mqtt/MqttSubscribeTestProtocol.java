package com.iteaj.iot.test.mqtt;

import com.iteaj.iot.Message;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.test.TestConst;
import com.iteaj.iot.test.TestProtocolType;
import com.iteaj.iot.utils.ByteUtil;
import io.netty.handler.codec.mqtt.MqttQoS;

/**
 * mqtt订阅测试协议
 */
public class MqttSubscribeTestProtocol extends ServerInitiativeProtocol<MqttClientTestMessage> {

    public MqttSubscribeTestProtocol(MqttClientTestMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected void doBuildRequestMessage(MqttClientTestMessage requestMessage) {
        Message.MessageHead head = requestMessage.getHead();
        TestProtocolType type = head.getType();

        // 遗嘱主题订阅
        if(type == TestProtocolType.WillTop) {
            byte[] message = requestMessage.getMessage();
            logger.info(TestConst.LOGGER_PROTOCOL_DESC, "mqtt", "遗嘱测试["+new String(message)+"]"
                    , head.getEquipCode(), head.getMessageId(), "通过");
        } else {
            String topic = requestMessage.getTopic();
            switch (topic) {
                case MqttClientTestHandle.EXACTLY_ONCE_TOPIC:
                    logger.info(TestConst.LOGGER_PROTOCOL_DESC, "mqtt协议", MqttQoS.EXACTLY_ONCE
                            , head.getEquipCode(), head.getMessageId(), "通过");
                    break;
                case MqttClientTestHandle.AT_LEAST_ONCE_TOPIC:
                    logger.info(TestConst.LOGGER_PROTOCOL_DESC, "mqtt协议", MqttQoS.AT_LEAST_ONCE
                            , head.getEquipCode(), head.getMessageId(), "通过");
                    break;
                case MqttClientTestHandle.AT_MOST_ONCE_TOPIC:
                    logger.info(TestConst.LOGGER_PROTOCOL_DESC, "mqtt协议", MqttQoS.AT_MOST_ONCE
                            , head.getEquipCode(), head.getMessageId(), "通过");
                    break;
            }
        }
    }

    @Override
    protected MqttClientTestMessage doBuildResponseMessage() {
        return null;
    }

    @Override
    public ProtocolType protocolType() {
        return TestProtocolType.PIReq;
    }
}
