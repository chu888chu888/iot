package com.iteaj.iot.test.udp;

import com.iteaj.iot.server.ServerMessage;

/**
 * create time: 2021/9/12
 *
 * @author iteaj
 * @since 1.0
 */
public class UdpTestMessage extends ServerMessage {
    public UdpTestMessage(byte[] message) {
        super(message);
    }

    public UdpTestMessage(MessageHead head) {
        super(head);
    }

    public UdpTestMessage(MessageHead head, MessageBody body) {
        super(head, body);
    }

    @Override
    protected MessageHead doBuild(byte[] message) {
        return null;
    }
}
