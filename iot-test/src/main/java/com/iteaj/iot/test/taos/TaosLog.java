package com.iteaj.iot.test.taos;

import java.util.Date;

public class TaosLog {

    private Date ts;

    private Integer num;

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
}
