package com.iteaj.iot.test.modbus.dtu;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.test.TestProtocolType;

/**
 * 模拟modbus 主节点协议
 */
public class ModbusDtuClientTestProtocol extends ServerInitiativeProtocol<ModbusDtuClientTestMessage> {

    public ModbusDtuClientTestProtocol(ModbusDtuClientTestMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected void doBuildRequestMessage(ModbusDtuClientTestMessage requestMessage) {
        System.out.println(requestMessage);
    }

    @Override
    protected ModbusDtuClientTestMessage doBuildResponseMessage() {
        return null;
    }

    @Override
    public ProtocolType protocolType() {
        return TestProtocolType.PIReq;
    }
}
