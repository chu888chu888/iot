package com.iteaj.iot.test.modbus;

import com.iteaj.iot.modbus.client.message.ModbusTcpClientMessage;

public class ModbusTestMessage extends ModbusTcpClientMessage {

    public ModbusTestMessage(byte[] message) {
        super(message);
    }

    @Override
    public String getEquipCode() {
        return null;
    }
}
