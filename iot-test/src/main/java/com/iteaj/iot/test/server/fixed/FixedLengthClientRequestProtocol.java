package com.iteaj.iot.test.server.fixed;

import com.iteaj.iot.DeviceManager;
import com.iteaj.iot.Message;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.server.manager.DevicePipelineManager;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.test.MessageCreator;
import com.iteaj.iot.test.TestProtocolType;

public class FixedLengthClientRequestProtocol extends ClientInitiativeProtocol<FixedLengthServerMessage> {

    public FixedLengthClientRequestProtocol(FixedLengthServerMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected FixedLengthServerMessage doBuildResponseMessage() {
        Message.MessageHead head = requestMessage().getHead();
        String equipCode = head.getEquipCode();
        String messageId = head.getMessageId();
        DeviceManager instance = DevicePipelineManager.getInstance(FixedLengthServerMessage.class);
        return MessageCreator.buildFixedLengthServerMessage(equipCode, messageId, instance.useSize(), head.getType());
    }

    @Override
    protected void doBuildRequestMessage(FixedLengthServerMessage requestMessage) {

    }

    @Override
    public ProtocolType protocolType() {
        return TestProtocolType.CIReq;
    }
}
