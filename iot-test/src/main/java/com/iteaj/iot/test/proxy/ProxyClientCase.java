package com.iteaj.iot.test.proxy;

import com.iteaj.iot.client.ClientProperties;
import com.iteaj.iot.client.proxy.ProxyClientProtocol;
import com.iteaj.iot.proxy.ProxyClientJsonMessageBody;
import com.iteaj.iot.proxy.ProxyClientMessage;
import com.iteaj.iot.proxy.ProxyClientMessageHead;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.time.Duration;
import java.time.Instant;

public class ProxyClientCase implements InitializingBean {

    @Autowired
    private ClientProperties properties;
    @Autowired
    private ThreadPoolTaskScheduler scheduler;

    @Override
    public void afterPropertiesSet() throws Exception {
        scheduler.schedule(() -> {
            // 代理客户端参数解析测试
            ProxyClientMessage message = new ProxyClientMessage(new ProxyClientMessageHead()
                    , new ProxyClientJsonMessageBody("123456", "ctrl.test.param").add("status", "online"));
            new ProxyClientProtocol(message).request();

            // 代理客户端代理协议测试
            ProxyClientMessage sync = new ProxyClientMessage(new ProxyClientMessageHead(-1)
                    , new ProxyClientJsonMessageBody("123456", "ctrl.test.sync"));
            new ProxyClientProtocol(sync).request(protocol -> null);

            // 多客户端测试
//            ProxyClientMessage multi = new ProxyClientMessage(new ProxyClientMessageHead()
//                    , new ProxyClientJsonMessageBody("123456", "ctrl.test.multi")
//                    .add("status", "offline"));

//            Integer port = properties.getProxy().getPort();
//            new ProxyClientProtocol(multi).request("192.168.1.158", port);
        }, Instant.now().plusSeconds(10));
    }
}
