package com.iteaj.iot.test.udp;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.component.DatagramPacketDecoderComponent;

/**
 * create time: 2021/9/12
 *
 * @author iteaj
 * @since 1.0
 */
public class TestUdpDecoderComponent extends DatagramPacketDecoderComponent<UdpTestMessage> {
    public TestUdpDecoderComponent(ConnectProperties config) {
        super(config);
    }

    @Override
    public String getDesc() {
        return null;
    }

    @Override
    public AbstractProtocol getProtocol(UdpTestMessage message) {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

}
