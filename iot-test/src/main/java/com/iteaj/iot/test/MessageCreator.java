package com.iteaj.iot.test;

import com.iteaj.iot.Message;
import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.test.client.fixed.FixedLengthClientMessage;
import com.iteaj.iot.test.server.fixed.FixedLengthServerMessage;
import com.iteaj.iot.utils.ByteUtil;

/**
 * 报文创造器
 */
public class MessageCreator {

    /**
     * 固定长度报文 28
     *    8         8        4       8
     * 设备编号 + messageId + type + 递增值
     */
    public static Message.MessageHead buildFixedMessageHead(byte[] message) {
        String deviceSn = ByteUtil.bytesToLong(message, 0) + "";
        String messageId = ByteUtil.bytesToLong(message, 8) + "";
        int type = ByteUtil.bytesToInt(message, 16);

        TestProtocolType protocolType = null;
        switch (type) {
            case 0: protocolType = TestProtocolType.PIReq; break;
            case 1: protocolType = TestProtocolType.CIReq; break;
            case 2: protocolType = TestProtocolType.Heart; break;
        }

        return new DefaultMessageHead(deviceSn, messageId, protocolType);
    }

    public static FixedLengthClientMessage buildFixedLengthClientMessage(long deviceSn, long incVal, TestProtocolType protocolType) {
        DefaultMessageHead messageHead = buildFixedMessageHeader(deviceSn, ClientSnGen.getMessageId(), incVal, protocolType);
        return new FixedLengthClientMessage(messageHead);
    }

    public static FixedLengthServerMessage buildFixedLengthServerMessage(String equipCode, String messageId, long clientNum, TestProtocolType tradeType) {
        DefaultMessageHead defaultMessageHead = buildFixedMessageHeader(Long.valueOf(equipCode), messageId, clientNum, tradeType);
        return new FixedLengthServerMessage(defaultMessageHead);
    }

    /**
     * 固定长度报文 28
     *    8         8        4       8
     * 设备编号 + messageId + type + 递增值
     */
    private static DefaultMessageHead buildFixedMessageHeader(long deviceSn, String messageId, long incVal, TestProtocolType protocolType) {
        DefaultMessageHead messageHead = new DefaultMessageHead(deviceSn + "", messageId, TestProtocolType.Heart);

        int ordinal = protocolType.ordinal();
        byte[] type = ByteUtil.getBytes(ordinal); // 协议类型
        byte[] inc = ByteUtil.getBytes(incVal); // 递增值 心跳默认0
        byte[] bytes = ByteUtil.getBytes(deviceSn); // 设备编号
        byte[] msgId = ByteUtil.getBytes(Long.valueOf(messageId)); // msgId

        byte[] message = new byte[bytes.length + msgId.length + type.length + inc.length];

        ByteUtil.addBytes(message, bytes, 0);
        ByteUtil.addBytes(message, msgId, bytes.length);
        ByteUtil.addBytes(message, type, bytes.length + msgId.length);
        ByteUtil.addBytes(message, inc, bytes.length + msgId.length + type.length);

        messageHead.setMessage(message);
        return messageHead;
    }
}
