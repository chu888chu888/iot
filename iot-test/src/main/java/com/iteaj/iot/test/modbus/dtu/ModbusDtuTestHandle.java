package com.iteaj.iot.test.modbus.dtu;

import com.iteaj.iot.modbus.consts.ModbusCoilStatus;
import com.iteaj.iot.modbus.server.dtu.ModbusTcpForDtuCommonProtocol;
import com.iteaj.iot.server.ServerProtocolHandle;
import com.iteaj.iot.server.dtu.DtuDeviceSnProtocol;
import org.springframework.stereotype.Component;

@Component
public class ModbusDtuTestHandle implements ServerProtocolHandle<DtuDeviceSnProtocol> {

    @Override
    public Object handle(DtuDeviceSnProtocol protocol) {
        String equipCode = protocol.getEquipCode();
        ModbusTcpForDtuCommonProtocol.buildRead01(equipCode, 1, 1, 1).request(protocol1 -> null);
        ModbusTcpForDtuCommonProtocol.buildRead02(equipCode, 1, 2, 1).request(protocol1 -> null);
        ModbusTcpForDtuCommonProtocol.buildRead03(equipCode, 1, 3, 3).request(protocol1 -> null);

        ModbusTcpForDtuCommonProtocol.buildRead04(equipCode, 1, 3, 3).request(protocol1 -> {
            return null;
        });

        ModbusTcpForDtuCommonProtocol.buildWrite05(equipCode, 1, 1, ModbusCoilStatus.ON).request(protocol1 -> {
            return null;
        });

        ModbusTcpForDtuCommonProtocol.buildWrite06(equipCode, 1, 1, new byte[]{0x01, 0x02, 0x03}).request(protocol1 -> {
            return null;
        });

        return null;
    }
}
