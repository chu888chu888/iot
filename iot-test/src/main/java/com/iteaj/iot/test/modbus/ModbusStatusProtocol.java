package com.iteaj.iot.test.modbus;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.modbus.ModbusTcpMessageBuilder;
import com.iteaj.iot.modbus.client.message.ModbusTcpClientMessage;
import com.iteaj.iot.modbus.consts.ModbusCode;
import com.iteaj.iot.modbus.consts.ModbusCoilStatus;
import com.iteaj.iot.test.TestProtocolType;

public class ModbusStatusProtocol extends ClientInitiativeProtocol<ModbusTcpClientMessage> {

    private ModbusCode code;

    public ModbusStatusProtocol(ModbusCode code) {
        this.code = code;
    }

    @Override
    protected ModbusTcpClientMessage doBuildRequestMessage() {
        switch (code) {
            case Read01:
                return ModbusTcpMessageBuilder.buildRead01Message(new ModbusTcpClientMessage(), (byte) 0x01, 0, 16);
            case Read02:
                return ModbusTcpMessageBuilder.buildRead02Message(new ModbusTcpClientMessage(), (byte) 0x01, 0, 8);
            case Read03:
                return ModbusTcpMessageBuilder.buildRead03Message(new ModbusTcpClientMessage(), (byte) 0x01, 0, 16);
            case Read04:
                return ModbusTcpMessageBuilder.buildRead04Message(new ModbusTcpClientMessage(), (byte) 0x01, 0, 1);
            case Write05:
                return ModbusTcpMessageBuilder.buildWrite05Message(new ModbusTcpClientMessage(), (byte) 0x01, 0, ModbusCoilStatus.ON);
            case Write06:
                return ModbusTcpMessageBuilder.buildWrite06Message(new ModbusTcpClientMessage(), (byte) 0x01, 1, new byte[]{(byte) 0x00, (byte) 0x06});
            case Write0F:
                return ModbusTcpMessageBuilder.buildWrite0FMessage(new ModbusTcpClientMessage(), (byte) 0x01, 0, new byte[]{(byte) 0x01, (byte) 0x10});
            case Write10:
                return ModbusTcpMessageBuilder.buildWrite10Message(new ModbusTcpClientMessage(), (byte) 0x01, 2, 1, new byte[]{(byte) 0x00, (byte) 0x10});
            default: throw new IllegalStateException("不支持功能码["+code+"]");
        }
    }

    @Override
    public void doBuildResponseMessage(ModbusTcpClientMessage responseMessage) {

    }

    @Override
    public ProtocolType protocolType() {
        return TestProtocolType.CIReq;
    }
}
