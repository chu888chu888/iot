package com.iteaj.iot.test;

import com.iteaj.iot.utils.IdWorker;
import org.apache.commons.lang3.RandomUtils;

/**
 * 客户端编号生成器
 */
public class ClientSnGen {

    private static IdWorker worker = new IdWorker(RandomUtils.nextInt(0, 32), RandomUtils.nextInt(0, 32));

    /**
     * 获取客户端编号
     * @return
     */
    public static long getClientSn() {
        return worker.nextId();
    }

    public static String getMessageId() {
        return worker.nextId() + "";
    }

}
