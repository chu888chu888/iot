package com.iteaj.iot.test.server.fixed;

import com.iteaj.iot.consts.ExecStatus;
import com.iteaj.iot.redis.handle.RedisListHandle;
import com.iteaj.iot.server.ServerProtocolHandle;
import com.iteaj.iot.taos.TaosHandle;
import com.iteaj.iot.test.TestConst;
import com.iteaj.iot.test.TestEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.List;

/**
 * 固定长度+redis测试
 */
public class FixedLengthClientRequestHandle implements ServerProtocolHandle<FixedLengthClientRequestProtocol>
        , RedisListHandle<FixedLengthClientRequestProtocol, TestEntity>, TaosHandle<FixedLengthClientRequestProtocol> {

    private boolean testStatus = false;
    @Autowired
    private ThreadPoolTaskScheduler scheduler;
    @Autowired
    private TestFixedLengthDecoderComponent component;

    private Logger logger = LoggerFactory.getLogger(getClass());

    // 从redis消费数据
    @Override
    public Integer consumer(List<TestEntity> objects) {
        return objects.size();
    }

    // 此方法直接写业务
    @Override
    public Object handle(FixedLengthClientRequestProtocol protocol) {
        final String equipCode = protocol.getEquipCode();
        if(!testStatus) {
            testStatus = true; // 只测试一次
            scheduler.execute(() -> {
                final long id = Thread.currentThread().getId();
                new FixedLengthServerRequestProtocol(equipCode).sync(1000).request(protocol1 -> {
                    final long id1 = Thread.currentThread().getId();
                    if(protocol1 instanceof FixedLengthServerRequestProtocol) {
                        final String messageId = protocol1.requestMessage().getHead().getMessageId();
                        final ExecStatus execStatus = ((FixedLengthServerRequestProtocol) protocol1).getExecStatus();
                        if(execStatus == ExecStatus.timeout) {
                            // 同步请求 调用线程 == 执行线程
                            if(id == id1) {
                                logger.info(TestConst.LOGGER_PROTOCOL_DESC, component.getName()
                                        , "同步超时测试", equipCode, messageId, "通过");
                            } else {
                                logger.info(TestConst.LOGGER_PROTOCOL_DESC, component.getName()
                                        , "同步超时测试", equipCode, messageId, "失败");
                            }
                        }
                    }
                    return null;
                });

                new FixedLengthServerRequestProtocol(equipCode).timeout(1000).request(protocol2 -> {
                    final long id1 = Thread.currentThread().getId();
                    if(protocol2 instanceof FixedLengthServerRequestProtocol) {
                        final String messageId = protocol2.requestMessage().getHead().getMessageId();
                        final ExecStatus execStatus = ((FixedLengthServerRequestProtocol) protocol2).getExecStatus();
                        if(execStatus == ExecStatus.timeout) {
                            // 异步请求 调用线程 != 执行线程
                            if(id != id1) {
                                logger.info(TestConst.LOGGER_PROTOCOL_DESC, component.getName()
                                        , "异步超时测试", equipCode, messageId, "通过");
                            } else {
                                logger.info(TestConst.LOGGER_PROTOCOL_DESC, component.getName()
                                        , "异步超时测试", equipCode, messageId, "失败");
                            }
                        }
                    }
                    return null;
                });
            });
        }

        return new TestEntity(); // 返回的对象将直接存入redis
    }

    /**
     * 数据将写入到此Key的list里面
     * @return
     */
    @Override
    public String getKey() {
        return "Fixed_Redis_Key";
    }
}
