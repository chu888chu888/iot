package com.iteaj.iot.test.modbus.dtu;

import com.iteaj.iot.modbus.client.message.ModbusTcpClientMessage;

public class ModbusDtuClientTestMessage extends ModbusTcpClientMessage {

    public ModbusDtuClientTestMessage(byte[] message) {
        super(message);
    }

    @Override
    public String getEquipCode() {
        return null;
    }
}
