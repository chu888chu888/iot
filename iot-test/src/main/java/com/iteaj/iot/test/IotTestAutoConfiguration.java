package com.iteaj.iot.test;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.proxy.ProxyClientComponent;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.modbus.client.ModbusTcpClientComponent;
import com.iteaj.iot.modbus.server.dtu.ModbusTcpForDtuDecodeComponent;
import com.iteaj.iot.plc.omron.OmronComponent;
import com.iteaj.iot.plc.siemens.SiemensS7Component;
import com.iteaj.iot.test.client.fixed.FixedLengthClientComponent;
import com.iteaj.iot.test.client.fixed.FixedLengthRequestHandle;
import com.iteaj.iot.test.client.line.LineClientComponent;
import com.iteaj.iot.test.client.line.LineClientHandle;
import com.iteaj.iot.test.modbus.ModbusTestHandle;
import com.iteaj.iot.test.modbus.dtu.ModbusDtuClientTestComponent;
import com.iteaj.iot.test.mqtt.MqttClientTestComponent;
import com.iteaj.iot.test.mqtt.MqttClientTestHandle;
import com.iteaj.iot.test.plc.omron.OmronTcpTestHandle;
import com.iteaj.iot.test.plc.siemens.SiemensS7TestHandle;
import com.iteaj.iot.test.proxy.ProxyClientCase;
import com.iteaj.iot.test.server.fixed.FixedLengthClientRequestHandle;
import com.iteaj.iot.test.server.fixed.TestFixedLengthDecoderComponent;
import com.iteaj.iot.test.server.line.ServerLineBasedHandle;
import com.iteaj.iot.test.server.line.TestLineBasedFrameDecoderComponent;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * create time: 2021/9/4
 *
 * @author iteaj
 * @since 1.0
 */
@EnableConfigurationProperties(IotTestProperties.class)
public class IotTestAutoConfiguration {

    @Autowired
    private IotTestProperties properties;

    @Bean
    @ConditionalOnExpression("${iot.test.line.port:null}!=null and ${iot.test.server:false}")
    public TestLineBasedFrameDecoderComponent lineBasedFrameDecoderComponent() {
        ConnectProperties connectProperties = new ConnectProperties();
        BeanUtils.copyProperties(properties.getLine(), connectProperties, "host");
        return new TestLineBasedFrameDecoderComponent(connectProperties);
    }

    @Bean
    @ConditionalOnBean(TestLineBasedFrameDecoderComponent.class)
    public ServerLineBasedHandle serverLineBasedHandle() {
        return new ServerLineBasedHandle();
    }

    @Bean
    @ConditionalOnExpression("${iot.test.client:false} and ${iot.test.line.port:null}!=null")
    public LineClientComponent lineClientComponent() {
        return new LineClientComponent(properties.getLine());
    }

    @Bean
    @ConditionalOnBean(LineClientComponent.class)
    public LineClientHandle lineClientHandle() {
        return new LineClientHandle();
    }

    /**
     * 固定长度解码器服务端测试
     * @return
     */
    @Bean
    @ConditionalOnExpression("${iot.test.fixed.port:null}!=null and ${iot.test.server:false}")
    public TestFixedLengthDecoderComponent fixedLengthDecoderComponent() {
        ConnectProperties connectProperties = new ConnectProperties();
        BeanUtils.copyProperties(properties.getFixed(), connectProperties, "host");
        return new TestFixedLengthDecoderComponent(connectProperties);
    }

    @Bean
    @ConditionalOnBean(TestFixedLengthDecoderComponent.class)
    public FixedLengthClientRequestHandle fixedLengthClientRequestHandle() {
        return new FixedLengthClientRequestHandle();
    }

    /**
     * 固定长度解码器客户端测试
     * @return
     */
    @Bean
    @ConditionalOnExpression("${iot.test.client:false} and ${iot.test.fixed.port:null}!=null")
    public FixedLengthClientComponent fixedLengthClientComponent() {
        return new FixedLengthClientComponent(properties.getFixed());
    }

    @Bean
    @ConditionalOnBean(FixedLengthClientComponent.class)
    public FixedLengthRequestHandle fixedLengthRequestHandle() {
        return new FixedLengthRequestHandle();
    }

    /**
     * mqtt协议客户端测试组件
     * @return
     */
    @Bean
    @ConditionalOnExpression("${iot.test.client:false} and ${iot.test.mqtt.port:null}!=null")
    public MqttClientTestComponent mqttClientTestComponent() {
        return new MqttClientTestComponent(properties.getMqtt());
    }

    @Bean
    @ConditionalOnBean(MqttClientTestComponent.class)
    public MqttClientTestHandle mqttClientTestHandle() {
        return new MqttClientTestHandle();
    }

    /**
     * 西门子S7-200组件
     * @return
     */
    @Bean
    @ConditionalOnExpression("${iot.test.client:false} and ${iot.test.siemens.start:false}")
    public SiemensS7Component siemensS7Component() {
        return new SiemensS7Component(properties.getSiemens());
    }

    @Bean
    @ConditionalOnBean(SiemensS7Component.class)
    public SiemensS7TestHandle siemensS7TestHandle() {
        return new SiemensS7TestHandle();
    }

    @Bean
    @ConditionalOnExpression("${iot.test.client:false} and ${iot.test.omron.start:false}")
    public OmronComponent omronComponent() {
        return new OmronComponent(properties.getOmron());
    }

    @Bean
    @ConditionalOnBean(OmronComponent.class)
    public OmronTcpTestHandle omronTcpTestHandle() {
        return new OmronTcpTestHandle();
    }


    @Bean
    @ConditionalOnBean(ProxyClientComponent.class)
    public ProxyClientCase proxyClientCase() {
        return new ProxyClientCase();
    }

    /*********************************************  Modbus Tcp And DTU  *******************************/
    @Bean
//    @ConditionalOnBean(ModbusTcpClientComponent.class)
    public ModbusTestHandle modbusTestHandle() {
        return new ModbusTestHandle();
    }

    @Bean
    @ConditionalOnExpression("${iot.test.client:false} and ${iot.test.modbus.port:null}!=null")
    public ModbusTcpClientComponent modbusTcpClientComponent() {
        return new ModbusTcpClientComponent(properties.getModbus());
    }

    // Dtu For Server
    @Bean
    @ConditionalOnExpression("${iot.test.modbus-dtu.start:false} and ${iot.test.server:false}")
    public ModbusTcpForDtuDecodeComponent modbusTcpDtuDecodeComponent() {
        return new ModbusTcpForDtuDecodeComponent(properties.getModbusDtu());
    }

    // Dtu for Client
    @Bean
    @ConditionalOnExpression("${iot.test.modbus-dtu.start:false} and ${iot.test.server:false} and ${iot.test.client:false}")
    public ModbusDtuClientTestComponent dtuModbusClientTestComponent() {
        String host = properties.getModbusDtu().getHost();
        Integer port = properties.getModbusDtu().getPort();
        return new ModbusDtuClientTestComponent(new ClientConnectProperties(host, port));
    }
}
