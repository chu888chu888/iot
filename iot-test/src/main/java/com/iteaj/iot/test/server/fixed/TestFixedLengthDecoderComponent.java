package com.iteaj.iot.test.server.fixed;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.component.FixedLengthFrameDecoderComponent;
import com.iteaj.iot.server.protocol.HeartProtocol;
import com.iteaj.iot.test.TestProtocolType;

/**
 * 固定长度解码器组件测试
 */
public class TestFixedLengthDecoderComponent extends FixedLengthFrameDecoderComponent<FixedLengthServerMessage> {

    public TestFixedLengthDecoderComponent(ConnectProperties connectProperties) {
        super(connectProperties, 28);
    }

    @Override
    public String getDesc() {
        return "用于测试服务端固定长度字段解码器[FixedLengthFrameDecoder]";
    }

    @Override
    public AbstractProtocol getProtocol(FixedLengthServerMessage message) {
        TestProtocolType protocolType = message.getHead().getType();
        if(protocolType == TestProtocolType.Heart) {
            return HeartProtocol.getInstance(message);
        } else if(protocolType == TestProtocolType.CIReq) {
            return new FixedLengthClientRequestProtocol(message);
        } else {
            return remove(message.getHead().getMessageId());
        }

    }

    @Override
    public String getName() {
        return "固定长度字段解码";
    }

}
