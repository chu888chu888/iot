package com.iteaj.iot.test.client.line;

import com.iteaj.iot.Message;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.component.SingleTcpClientComponent;
import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.codec.IotMessageDecoder;
import com.iteaj.iot.codec.adapter.LineBasedFrameMessageDecoderAdapter;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.test.TestProtocolType;
import com.iteaj.iot.test.message.line.LineMessageHead;

public class LineClientComponent extends SingleTcpClientComponent<LineClientMessage> {

    public LineClientComponent(ClientConnectProperties config) {
        super(config);
    }

    @Override
    public String getName() {
        return "换行符解码";
    }

    @Override
    public AbstractProtocol getProtocol(LineClientMessage message) {
        LineMessageHead head = message.getHead();
        if(head.getType() == TestProtocolType.CIReq) {
            return remove(message.getMessageId());
        } else if(head.getType() == TestProtocolType.PIReq) {
            return new LineServerInitiativeProtocol(message);
        } else {
            return null;
        }
    }

    @Override
    protected IotMessageDecoder createProtocolDecoder() {
        return new LineBasedFrameMessageDecoderAdapter(1024);
    }
}
