package com.iteaj.iot.test.client.fixed;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.component.TcpClientComponent;
import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.test.TestMultiClientManager;
import com.iteaj.iot.test.TestProtocolType;

public class FixedLengthClientComponent extends TcpClientComponent<FixedLengthClientMessage> {

    public FixedLengthClientComponent(ClientConnectProperties config) {
        super(config, new TestMultiClientManager());
    }

    @Override
    public TcpSocketClient createNewClient(ClientConnectProperties config) {
        return new FixedLengthClient(this, config);
    }

    @Override
    public String getName() {
        return "固定长度字段解码";
    }

    @Override
    public String getDesc() {
        return "用于测试客户端固定长度解码器[FixedLengthFrameDecoder]";
    }

    @Override
    public AbstractProtocol getProtocol(FixedLengthClientMessage message) {
        TestProtocolType type = message.getHead().getType();
        if(type == TestProtocolType.CIReq) {
            return remove(message.getHead().getMessageId());
        }

        return null;
    }
}
