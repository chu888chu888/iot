package com.iteaj.iot.test.server.line.protocol;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.server.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.test.ServerInfoUtil;
import com.iteaj.iot.test.SystemInfo;
import com.iteaj.iot.test.TestProtocolType;
import com.iteaj.iot.test.message.line.LineMessage;
import com.iteaj.iot.test.message.line.LineMessageBody;
import com.iteaj.iot.test.message.line.LineMessageHead;

import java.io.IOException;

public class LineServerInitiativeProtocol extends ServerInitiativeProtocol<LineMessage> {

    private String equipCode;

    public LineServerInitiativeProtocol(String equipCode) {
        this.equipCode = equipCode;
    }

    @Override
    protected LineMessage doBuildRequestMessage() throws IOException {
        String messageId = ServerInfoUtil.getMessageId();
        SystemInfo systemInfo = ServerInfoUtil.getSystemInfo();
        return new LineMessage(LineMessageHead.build(this.equipCode
                , messageId, protocolType(), systemInfo), LineMessageBody.build());
    }

    @Override
    protected void doBuildResponseMessage(LineMessage message) {

    }

    @Override
    public ProtocolType protocolType() {
        return TestProtocolType.PIReq;
    }
}
