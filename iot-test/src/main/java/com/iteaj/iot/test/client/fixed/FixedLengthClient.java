package com.iteaj.iot.test.client.fixed;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.ClientComponent;
import com.iteaj.iot.codec.adapter.FixedLengthFrameDecoderAdapter;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.test.ClientSnGen;
import io.netty.channel.ChannelInboundHandler;

import java.util.function.Consumer;

public class FixedLengthClient extends TcpSocketClient {

    private long deviceSn;

    public FixedLengthClient(ClientComponent clientComponent, ClientConnectProperties config) {
        super(clientComponent, config);
        this.deviceSn = ClientSnGen.getClientSn();
    }

    @Override
    protected ChannelInboundHandler createProtocolDecoder() {
        return new FixedLengthFrameDecoderAdapter(28);
    }

    public long getDeviceSn() {
        return deviceSn;
    }
}
