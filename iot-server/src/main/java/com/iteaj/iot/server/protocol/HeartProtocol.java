package com.iteaj.iot.server.protocol;

import com.iteaj.iot.Message;
import com.iteaj.iot.protocol.CommonProtocolType;
import com.iteaj.iot.server.ServerComponent;
import com.iteaj.iot.server.ServerMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * create time: 2021/3/6
 *  通用的心跳协议
 *  此协议不响应客户端, 只做日志记录
 * @author iteaj
 * @since 1.0
 */
public class HeartProtocol extends ClientInitiativeProtocol<ServerMessage> {

    private ServerComponent serverComponent;
    private static Logger logger = LoggerFactory.getLogger(HeartProtocol.class);

    protected HeartProtocol(ServerMessage requestMessage) {
        super(requestMessage);
    }

    public static HeartProtocol getInstance(ServerMessage requestMessage) {
        return new HeartProtocol(requestMessage);
    }

    @Override
    public CommonProtocolType protocolType() {
        return CommonProtocolType.Common_Heart;
    }

    @Override
    protected ServerMessage doBuildResponseMessage() {
        return null;
    }

    public ServerComponent getServerComponent() {
        return serverComponent;
    }

    public HeartProtocol setServerComponent(ServerComponent serverComponent) {
        this.serverComponent = serverComponent;
        return this;
    }

    @Override
    protected void doBuildRequestMessage(ServerMessage requestMessage) {
        if(logger.isTraceEnabled()) {
            Message.MessageHead head = requestMessage.getHead();
            logger.trace("心跳({}) 设备编号：{} - messageId：{} 协议：{}", serverComponent.getName()
                    , head.getEquipCode(), head.getMessageId(), head.getType());
        }
    }
}
