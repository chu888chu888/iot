package com.iteaj.iot.server.dtu.message;

import com.iteaj.iot.Message;

public interface DtuMessage extends Message {

    String getEquipCode();

    void setEquipCode(String equipCode);

    /**
     * 构建Dtu设备的第一包设备编号报文头
     * @return
     */
    MessageHead buildFirstHead();

}
