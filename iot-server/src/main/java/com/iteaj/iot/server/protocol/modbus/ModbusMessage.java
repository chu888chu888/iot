package com.iteaj.iot.server.protocol.modbus;

import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.message.UnParseBodyMessage;

public class ModbusMessage extends UnParseBodyMessage {

    public ModbusMessage(byte[] message) {
        super(message);
    }

    @Override
    protected DefaultMessageHead doBuild(byte[] message) {
        return null;
    }
}
