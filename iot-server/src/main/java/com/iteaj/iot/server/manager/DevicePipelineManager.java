package com.iteaj.iot.server.manager;

import com.iteaj.iot.*;
import com.iteaj.iot.server.ServerMessage;
import io.netty.channel.*;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.EventExecutor;
import io.netty.util.internal.PlatformDependent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentMap;

/**
 * <p>通过设备编号获取以设备关联的链接信息{@link ChannelPipeline}</p>
 *  映射关系: key：为设备编号   value：{@link ChannelPipeline}
 * Create Date By 2017-09-12
 * @author iteaj
 * @since 1.7
 */
public class DevicePipelineManager extends DefaultChannelGroup implements DeviceManager {

    /**
     * 存储客户端编号和对应的连接
     */
    private final ConcurrentMap<String, Channel> clientChannels = PlatformDependent.newConcurrentHashMap();
    private static Logger logger = LoggerFactory.getLogger(DevicePipelineManager.class);

    public DevicePipelineManager(String name, EventExecutor executor) {
        super(name, executor);
    }

    public DevicePipelineManager(String name, EventExecutor executor, boolean stayClosed) {
        super(name, executor, stayClosed);
    }

    /**
     * 获取一个设备管理实例
     * @return
     */
    public static DeviceManager getInstance(Class<? extends ServerMessage> clazz){
        return IotServeBootstrap.getServerComponent(clazz).getDeviceManager();
    }

    @Override
    public boolean add(String equipCode, Channel channel) {
        return clientChannels.putIfAbsent(equipCode, channel) == null;
    }

    @Override
    public int useSize() {
        return clientChannels.size();
    }

    @Override
    public Channel find(String equipCode) {
        return clientChannels.get(equipCode);
    }

    @Override
    public boolean isEmpty() {
        return clientChannels.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        if(o instanceof String) {
            return clientChannels.containsKey(o);
        }

        return super.contains(o);
    }

    @Override
    public boolean remove(Object o) {
        if(o instanceof Channel) {
            // 移除对应的客户端
            Object deviceSn = ((Channel) o).attr(CoreConst.EQUIP_CODE).get();
            if(deviceSn instanceof String) {
                clientChannels.remove(deviceSn);
            }
        }

        return super.remove(o);
    }

    @Override
    public void clear() {
        super.clear();
        clientChannels.clear();
    }

    @Override
    public ChannelFuture writeAndFlush(String equipCode, Object msg, Object... args) {
        if(msg instanceof Protocol) {
            return this.writeAndFlush(equipCode, (Protocol) msg);
        } else {
            if(StringUtils.isBlank(equipCode))
                throw new IllegalArgumentException("设备编号不能为空");
            if(null == msg) {
                throw new IllegalArgumentException("请传入要发送的协议报文");
            }

            Channel channel = find(equipCode);
            if(null == channel) {
                logger.warn("设备在线管理({}) 无此设备或设备断线 - 设备编号: {}", this.size(), equipCode);
                return null;

            } else if(!channel.isActive()) { // 设备已经取消注册, 删除设备
                remove(equipCode);
                logger.warn("设备在线管理({}) 设备断线 - 设备编号: {}", this.size(), equipCode);
                return null;
            }

            return channel.writeAndFlush(msg);
        }
    }

    @Override
    public ChannelFuture writeAndFlush(String equipCode, Protocol protocol) {
        if(StringUtils.isBlank(equipCode))
            throw new IllegalArgumentException("设备编号不能为空");
        if(null == protocol) {
            throw new IllegalArgumentException("请传入要发送的协议报文");
        }

        Channel channel = find(equipCode);
        if(null == channel) {
            logger.warn("设备在线管理({}) 无此设备或设备断线 - 设备编号: {} - 协议: {}"
                    , this.size(), equipCode, protocol.protocolType());

            return null;
        } else if(!channel.isActive()) { // 设备已经取消注册, 删除设备
            remove(equipCode);
            logger.warn("设备在线管理({}) 设备断线 - 设备编号: {} - 协议: {}"
                    , this.size(), equipCode, protocol.protocolType());
            return null;
        }

        return channel.writeAndFlush(protocol);
    }
}
