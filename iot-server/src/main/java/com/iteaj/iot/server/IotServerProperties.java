package com.iteaj.iot.server;

import com.iteaj.iot.config.ConnectProperties;
import io.netty.handler.logging.LogLevel;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static io.netty.handler.logging.LogLevel.DEBUG;
import static io.netty.handler.logging.LogLevel.WARN;

@ConfigurationProperties(prefix = "iot.server")
public class IotServerProperties {

    /**
     * netty Selector boss组线程数量
     */
    private short bossThreadNum = 1;

    /**
     * 应用程序客户端配置信息
     */
    private ProxyServerConnectProperties proxy = new ProxyServerConnectProperties();

    public short getBossThreadNum() {
        return bossThreadNum;
    }

    public void setBossThreadNum(short bossThreadNum) {
        this.bossThreadNum = bossThreadNum;
    }

    public ProxyServerConnectProperties getProxy() {
        return proxy;
    }

    public IotServerProperties setProxy(ProxyServerConnectProperties proxy) {
        this.proxy = proxy;
        return this;
    }

    public static class ProxyServerConnectProperties extends ConnectProperties {

        /**
         * 是否启用客户端端口监听
         */
        private boolean start = true;

        public ProxyServerConnectProperties() {
            super(30168);
        }

        public boolean isStart() {
            return start;
        }

        public void setStart(boolean start) {
            this.start = start;
        }
    }
}
