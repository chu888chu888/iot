package com.iteaj.iot.server.dtu;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.CoreConst;
import com.iteaj.iot.IotProtocolFactory;
import com.iteaj.iot.ProtocolException;
import com.iteaj.iot.codec.SocketMessageDecoder;
import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.server.IotSocketServer;
import com.iteaj.iot.server.ServerMessage;
import com.iteaj.iot.server.dtu.message.DtuMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;

/**
 * DTU设备报文解码
 * @param <M>
 */
public interface DtuMessageDecoder<M extends ServerMessage> extends SocketMessageDecoder<M, ByteBuf>, IotProtocolFactory<M>, IotSocketServer {

    @Override
    default M createMessage(byte[] message) {
        M serverMessage = SocketMessageDecoder.super.createMessage(message);
        if(serverMessage instanceof DtuMessage) {
            return serverMessage;
        } else {
            throw new ProtocolException("Dtu报文对象必须是["+DtuMessage.class.getSimpleName()+"]的子类");
        }
    }

    @Override
    Class<M> getMessageClass();

    @Override
    default void doInitChannel(ChannelPipeline p) {
        IotSocketServer.super.doInitChannel(p);
        p.addBefore(CoreConst.SERVER_DECODER_HANDLER, "DtuFirstDeviceSnPackageHandler", new DtuFirstDeviceSnPackageHandler(this));
    }

    @Override
    default M decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        M decode = SocketMessageDecoder.super.decode(ctx, in);
        Object equipCode = ctx.channel().attr(CoreConst.EQUIP_CODE).get();

        // 设置Dtu报文的设备编号
        if(equipCode != null) {
            ((DtuMessage) decode).setEquipCode((String) equipCode);
        }

        return decode;
    }

    default AbstractProtocol getProtocol(M message) {
        DefaultMessageHead head = (DefaultMessageHead) message.getHead();

        // Dtu获取设备编号的报文使用默认的协议来处理{@link DtuDeviceSnProtocol}
        if(head != null && head.getType() == DtuCommonProtocolType.DEVICE_SN) {
            return new DtuDeviceSnProtocol(message);
        }

        // 获取自定义业务协议
        return doGetProtocol(message);
    }

    /**
     * 获取对应的协议
     * @param message
     * @return
     */
    AbstractProtocol doGetProtocol(M message);
}
