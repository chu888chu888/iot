package com.iteaj.iot.server.dtu;

import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.ServerMessage;
import com.iteaj.iot.server.component.LengthFieldBasedFrameDecoderComponent;

import java.nio.ByteOrder;

/**
 * 使用长度字段解码器协议并且使用Dtu连网
 * @see DtuMessageDecoder
 * @see DtuFirstDeviceSnPackageHandler
 * @see LengthFieldBasedFrameDecoderComponent
 * @param <M>
 */
public abstract class LengthFieldBasedFrameForDtuDecoderComponent<M extends ServerMessage> extends LengthFieldBasedFrameDecoderComponent<M> implements DtuMessageDecoder<M> {

    public LengthFieldBasedFrameForDtuDecoderComponent(ConnectProperties connectProperties, int maxFrameLength, int lengthFieldOffset, int lengthFieldLength) {
        super(connectProperties, maxFrameLength, lengthFieldOffset, lengthFieldLength);
    }

    public LengthFieldBasedFrameForDtuDecoderComponent(ConnectProperties connectProperties, int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip) {
        super(connectProperties, maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip);
    }

    public LengthFieldBasedFrameForDtuDecoderComponent(ConnectProperties connectProperties, int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip, boolean failFast) {
        super(connectProperties, maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip, failFast);
    }

    public LengthFieldBasedFrameForDtuDecoderComponent(ConnectProperties connectProperties, ByteOrder byteOrder, int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip, boolean failFast) {
        super(connectProperties, byteOrder, maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip, failFast);
    }
}
