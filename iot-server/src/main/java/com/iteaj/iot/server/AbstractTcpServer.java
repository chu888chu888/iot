package com.iteaj.iot.server;

import com.iteaj.iot.ProtocolException;
import com.iteaj.iot.config.ConnectProperties;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

public abstract class AbstractTcpServer implements IotSocketServer {

    private int port;
    private ConnectProperties config;

    /**
     * @see #AbstractTcpServer(ConnectProperties)
     * @param port
     */
    @Deprecated
    public AbstractTcpServer(int port) {
        this(new ConnectProperties(port));
    }

    public AbstractTcpServer(ConnectProperties serverConfig) {
        this.config = serverConfig;
        this.port = serverConfig.getPort();
    }

    @Override
    public int port() {
        return this.port;
    }

    @Override
    public ConnectProperties config() {
        return this.config;
    }

}
