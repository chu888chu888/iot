package com.iteaj.iot.server.dtu.message;

import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.server.ServerMessage;

public abstract class DtuMessageAbstract extends ServerMessage implements DtuMessage {

    /**
     * 设备编号
     */
    private String equipCode;

    public DtuMessageAbstract(byte[] message) {
        super(message);
    }

    public DtuMessageAbstract(DefaultMessageHead head) {
        super(head);
    }

    public DtuMessageAbstract(DefaultMessageHead head, MessageBody body) {
        super(head, body);
    }

    public String getEquipCode() {
        return equipCode;
    }

    public void setEquipCode(String equipCode) {
        this.equipCode = equipCode;
    }
}
