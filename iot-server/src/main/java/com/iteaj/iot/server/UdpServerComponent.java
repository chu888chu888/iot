package com.iteaj.iot.server;

import com.iteaj.iot.ProtocolTimeoutStorage;

public abstract class UdpServerComponent extends ServerComponent {

    /**
     * Udp协议一般不需要进行同步调用, 去除协议的超时存储
     * @return
     */
    @Override
    protected ProtocolTimeoutStorage doCreateProtocolTimeoutStorage() {
        return null;
    }
}
