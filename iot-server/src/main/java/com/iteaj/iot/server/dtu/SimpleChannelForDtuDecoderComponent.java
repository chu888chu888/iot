package com.iteaj.iot.server.dtu;

import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.component.SimpleChannelDecoderComponent;
import com.iteaj.iot.server.dtu.message.DtuMessageAbstract;

/**
 * Dtu 简单通道解码器
 * @see DtuMessageDecoder
 * @see DtuFirstDeviceSnPackageHandler dtu的第一包上报设备编号
 */
public abstract class SimpleChannelForDtuDecoderComponent<M extends DtuMessageAbstract> extends SimpleChannelDecoderComponent<M> implements DtuMessageDecoder<M> {

    public SimpleChannelForDtuDecoderComponent(ConnectProperties connectProperties) {
        super(connectProperties);
    }

}
