package com.iteaj.iot.redis.proxy;

import com.iteaj.iot.redis.producer.RedisProducer;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class ProtocolHandleProxyBeanPostProcessor implements BeanPostProcessor {

    private ProtocolHandleProxy handleProxy = new ProtocolHandleProxy();

    /**
     *
     * @see com.iteaj.iot.ProtocolHandle
     * @see RedisProducer 对所有的RedisProducer进行代理
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof RedisProducer) {
            return handleProxy.createProxy((RedisProducer) bean);
        }

        return bean;
    }
}
