package com.iteaj.iot.redis;

import com.alibaba.fastjson.support.spring.FastJsonRedisSerializer;
import com.iteaj.iot.redis.consumer.ListConsumer;
import com.iteaj.iot.redis.consumer.ListConsumerOpera;
import com.iteaj.iot.redis.consumer.RedisConsumerOpera;
import com.iteaj.iot.redis.consumer.RedisConsumerOperaManager;
import com.iteaj.iot.redis.proxy.ProtocolHandleProxyBeanPostProcessor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.List;

@EnableConfigurationProperties(IotRedisProperties.class)
@AutoConfigureBefore(name = "org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration")
public class IotRedisConfiguration implements BeanFactoryPostProcessor, ApplicationContextAware {

    private Boolean producer;
    private ApplicationContext applicationContext;

    @Bean(name = "iotRedisTemplate")
    @ConditionalOnMissingBean(name = "iotRedisTemplate")
    public RedisTemplate<String, Object> iotRedisTemplate(RedisConnectionFactory factory) {
        // 创建RedisTemplate<String, Object>对象
        RedisTemplate<String, Object> template = new RedisTemplate<>();

        // 配置连接工厂
        template.setConnectionFactory(factory);

        // FastJsonRedisSerializer
        FastJsonRedisSerializer<Object> fastJsonRedisSerializer = new FastJsonRedisSerializer<>(Object.class);

        StringRedisSerializer stringSerial = new StringRedisSerializer();

        // redis key 序列化方式使用stringSerial
        template.setKeySerializer(stringSerial);
        // redis value 序列化方式使用jackson
        template.setValueSerializer(fastJsonRedisSerializer);
        // redis hash key 序列化方式使用stringSerial
        template.setHashKeySerializer(stringSerial);
        // redis hash value 序列化方式使用jackson
        template.setHashValueSerializer(fastJsonRedisSerializer);

        return IotRedis.wrapper.template = template;
    }

    /**
     * 开启消费者任务执行管理器
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "iot.redis", value = "consumer", havingValue = "true")
    public RedisConsumerOperaManager redisConsumerOperaManager(List<RedisConsumerOpera> operas
            , ThreadPoolTaskExecutor executor, RedisProperties redisProperties) {
        return new RedisConsumerOperaManager(operas, executor, redisProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnBean(value = {ListConsumer.class, RedisConsumerOperaManager.class})
    public ListConsumerOpera listConsumerOpera(List<ListConsumer> consumers) {
        return new ListConsumerOpera(consumers);
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        if(this.producer) {
            beanFactory.addBeanPostProcessor(new ProtocolHandleProxyBeanPostProcessor());
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
        this.producer = this.applicationContext.getEnvironment()
                .getProperty("iot.redis.producer", Boolean.class, false);
    }
}
