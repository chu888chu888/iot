package com.iteaj.iot;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

/**
 * create time: 2022/1/16
 *
 * @author iteaj
 * @since 1.0
 */
@PropertySource("/IOTCore.properties")
@EnableConfigurationProperties(IotCoreProperties.class)
public class IotCoreConfiguration {

    private final IotCoreProperties properties;

    public IotCoreConfiguration(IotCoreProperties properties) {
        this.properties = properties;
    }

    @Bean
    public IotThreadManager iotThreadManager() {
        return IotThreadManager.instance();
    }
}
