package com.iteaj.iot.client;

import com.iteaj.iot.config.ConnectProperties;

/**
 * 客户端连接配置
 */
public class ClientConnectProperties extends ConnectProperties {

    /**
     * 断开重连时间周期(秒)
     * 用于客户端
     */
    private long reconnect = 60;

    /**
     * 客户端唯一标识key
     * @see MultiClientManager#getClient(Object)
     */
    private String connectKey;

    public ClientConnectProperties() { }

    public ClientConnectProperties(Integer port) {
        this("127.0.0.1", port);
    }

    public ClientConnectProperties(String host, Integer port) {
        super(host, port);
    }

    public ClientConnectProperties(String host, Integer port, String connectKey) {
        super(host, port);
        this.connectKey = connectKey;
    }

    public ClientConnectProperties(Integer port, long allIdleTime, long readerIdleTime, long writerIdleTime) {
        this("127.0.0.1", port, allIdleTime, readerIdleTime, writerIdleTime);
    }

    public ClientConnectProperties(String host, Integer port, long allIdleTime, long readerIdleTime, long writerIdleTime) {
        super(host, port, allIdleTime, readerIdleTime, writerIdleTime);
    }

    /**
     * 使用主机和端口号来标识客户端的唯一性
     * @see MultiClientManager
     * @return
     */
    public String connectKey() {
        if(connectKey == null) {
            this.connectKey = getHost() + ":" + getPort();
        }

        return connectKey;
    }

    public ClientConnectProperties setConnectKey(String connectKey) {
        this.connectKey = connectKey;
        return this;
    }

    public long getReconnect() {
        return reconnect;
    }

    public void setReconnect(long reconnect) {
        this.reconnect = reconnect;
    }

    @Override
    public void setPort(Integer port) {
        super.setPort(port);
    }

    @Override
    public void setHost(String host) {
        super.setHost(host);
    }

    @Override
    public String toString() {
        return this.connectKey();
    }
}
