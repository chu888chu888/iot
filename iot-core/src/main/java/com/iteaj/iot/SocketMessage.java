package com.iteaj.iot;

import com.iteaj.iot.message.VoidMessageBody;
import com.iteaj.iot.utils.ByteUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;

import java.io.IOException;

/**
 * Socket报文
 * <p>此报文未进行报文体解析, 将在如下两个地方解析对应的报文体 包括{@link #readBuild()} 和 {@link #writeBuild()}</p>
 * @see AbstractProtocol#buildRequestMessage() 在协议里面解析对应的报文体
 * @see AbstractProtocol#buildResponseMessage() 在协议里面解析对应的报文体
 * 要求一种设备类型需要对应一种报文 如下：
 * {@code ServerComponentFactory#getByClass(SocketMessage)} 通过不同的报文获取相应的组件
 * Create Date By 2017-09-11
 * @author iteaj
 * @since 1.7
 */
public abstract class SocketMessage implements Message {

    /**
     * 二进制报文
     * -------   -------
     * header  +  body
     * -------   -------
     */
    protected byte[] message;

    /**
     * @see #VOID_MESSAGE_BODY 空报文体
     */
    protected MessageBody messageBody;

    /**
     * 报文头必须存在
     */
    protected MessageHead messageHead;

    /** 日志 */
    protected Logger logger = LoggerFactory.getLogger(getClass());

    /**空报文体*/
    public static final VoidMessageBody VOID_MESSAGE_BODY = VoidMessageBody.getInstance();

    /**
     * 通过报文内容创建一个报文对象(一般用于接受客户端的请求或者响应)
     * @see #doBuild(byte[])
     * @param message
     */
    public SocketMessage(byte[] message) {
        this.message = message;
    }

    /**
     * 通过报文头和报文体构建报文(一般用于平台主动请求或者响应客户端)
     * @see #VOID_MESSAGE_BODY 默认是用空报文体
     * @see #writeBuild()
     * @param messageHead
     */
    public SocketMessage(@NonNull MessageHead messageHead) {
        this(messageHead, VOID_MESSAGE_BODY);
    }

    /**
     * 通过报文头和报文体构建报文(一般用于平台主动请求或者响应客户端)
     * @see #writeBuild()
     * @param messageHead
     * @param messageBody
     */
    public SocketMessage(@NonNull MessageHead messageHead, @NonNull MessageBody messageBody) {
        this.messageBody = messageBody;
        this.messageHead = messageHead;
    }

    /**
     * 用于将{@link #message} 解析成报文头和报文体
     * @see #messageHead 解析的报文头必须不为空, 需要包含 type、messageId等
     * @see #messageBody 可空
     * 在报文编解码的时候会被调用 {@code DeviceProtocolEncoder} or {@code ClientRequestEncoder}
     * @see Message#getMessage() 报文编码的时候如果此方法返回{@code null}才会被调用
     * @return 解析后的报文 or this
     * @throws IOException
     * @deprecated @see {@link #readBuild()} and writeBuild
     */
    @Deprecated
    public SocketMessage build() throws ProtocolException {
        // 如果{@code message}不为空默认是客户端请求的报文, 将二进制报文转成实体报文
        if(this.message != null) {
            this.readBuild();
            return this;
        }

        // 如果实体报文头和报文体存在默认将报文头和报文体的实体报文合并到二进制报文
        // 如果不需要{@like MessageBody} 请设置{@code SocketMessage#VOID_MESSAGE_BODY}
        if(this.getHead() != null && this.getBody() != null) {
            writeBuild();
            return this;
        }

        // 其他情况声明异常
        throw new ProtocolException("错误的报文[message == null or head == null or body == null]");
    }

    /**
     * 从二进制报文{@link #message}解析出报文头{@link #messageHead}
     */
    public SocketMessage readBuild() {
        this.messageHead = doBuild(this.message);
        this.headValidate(this.messageHead);
        return this;
    }

    /**
     * 将报文头{@link #messageHead}和报文体{@link #messageBody}的内容合并成二进制{@link #message}报文
     */
    public void writeBuild() {
        // 创建报文对象
        int length = this.messageHead.getLength() + this.messageBody.getLength();
        this.message = new byte[length];
        ByteUtil.addBytes(message, this.messageHead.getMessage(), 0);
        ByteUtil.addBytes(message, this.messageBody.getMessage(), this.messageHead.getLength());
    }

    /**
     * 校验报文头, 报文头不能为null
     * @see #doBuild(byte[]) 必须返回报文头对象
     * @param head
     */
    protected void headValidate(@NonNull MessageHead head) {
        if(head == null) {
            throw new IllegalArgumentException("未解析出报文头对象[#doBuild(byte[])]");
        }
    }


    /**
     * 解析出message里面的报文头对象
     * @see #message {@link #messageHead}
     * @param message
     */
    protected abstract MessageHead doBuild(byte[] message);

    @Override
    public int length() {
        return message.length;
    }

    @Override
    public byte[] getMessage() {
        return message;
    }

    @Override
    public MessageHead getHead() {
        return messageHead;
    }

    /**
     * 返回 {@link VoidMessageBody}
     * @return
     */
    @Override
    public MessageBody getBody() {
        return messageBody;
    }

    public void setBody(MessageBody messageBody) {
        this.messageBody = messageBody;
    }

    public void setHead(MessageHead messageHead) {
        this.messageHead = messageHead;
    }

    public void setMessage(byte[] message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message != null ? ByteUtil.bytesToHex(message) : "null";
    }
}
