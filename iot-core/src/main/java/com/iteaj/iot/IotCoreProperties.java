package com.iteaj.iot;

import io.netty.handler.logging.LogLevel;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static io.netty.handler.logging.LogLevel.DEBUG;

/**
 * create time: 2022/1/16
 *
 * @author iteaj
 * @since 1.0
 */
@ConfigurationProperties(prefix = "iot.core")
public class IotCoreProperties {

    /**
     * netty 打印的日志级别
     */
    private LogLevel level = DEBUG;

    /**
     * netty 工作组线程数量
     */
    private short workerThreadNum = 3;

    /**
     * Iot框架核心线程数
     */
    private short coreThreadNum = 1;

    public LogLevel getLevel() {
        return level;
    }

    public IotCoreProperties setLevel(LogLevel level) {
        this.level = level;
        return this;
    }

    public short getWorkerThreadNum() {
        return workerThreadNum;
    }

    public IotCoreProperties setWorkerThreadNum(short workerThreadNum) {
        this.workerThreadNum = workerThreadNum;
        return this;
    }

    public short getCoreThreadNum() {
        return coreThreadNum;
    }

    public IotCoreProperties setCoreThreadNum(short coreThreadNum) {
        this.coreThreadNum = coreThreadNum;
        return this;
    }
}
