package com.iteaj.iot.event;

/**
 * Create Date By 2017-09-29
 * @author iteaj
 * @since 1.7
 */
public enum DeviceEventType {
    offline, // 掉线
    online; // 上线
}
