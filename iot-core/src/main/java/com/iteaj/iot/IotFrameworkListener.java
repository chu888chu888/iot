package com.iteaj.iot;

import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * create time: 2022/1/17
 *
 * @author iteaj
 * @since 1.0
 */
public class IotFrameworkListener implements ApplicationListener<ApplicationStartedEvent> {

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        final ConfigurableApplicationContext context = event.getApplicationContext();
        final String version = context.getEnvironment().getProperty("iot.version");

        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("|                                                                              |");
        System.out.println("|                     STARTED IOT FRAMEWORK BY NETTY (v"+version+")                  |");
        System.out.println("|                             (welcome to apply IOT)                           |");
        System.out.println("|                                                                              |");
        System.out.println("--------------------------------------------------------------------------------");
    }
}
