package com.iteaj.iot.codec.adapter;

import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.codec.IotMessageDecoder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LineBasedFrameDecoder;

public class LineBasedFrameMessageDecoderAdapter extends LineBasedFrameDecoder implements IotMessageDecoder {

    private Class<? extends SocketMessage> messageClass;

    public LineBasedFrameMessageDecoderAdapter(int maxLength) {
        super(maxLength);
    }

    public LineBasedFrameMessageDecoderAdapter(int maxLength, boolean stripDelimiter, boolean failFast) {
        super(maxLength, stripDelimiter, failFast);
    }

    public LineBasedFrameMessageDecoderAdapter(int maxLength, Class<? extends SocketMessage> messageClass) {
        super(maxLength);
        this.messageClass = messageClass;
    }

    public LineBasedFrameMessageDecoderAdapter(int maxLength, boolean stripDelimiter
            , boolean failFast, Class<? extends SocketMessage> messageClass) {
        super(maxLength, stripDelimiter, failFast);
        this.messageClass = messageClass;
    }

    @Override
    public Class<? extends SocketMessage> getMessageClass() {
        return this.messageClass;
    }

    @Override
    public IotMessageDecoder setMessageClass(Class<? extends SocketMessage> messageClass) {
        this.messageClass = messageClass;
        return this;
    }

    @Override
    public SocketMessage decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        return this.proxy(ctx, (ByteBuf) super.decode(ctx, in));
    }
}
