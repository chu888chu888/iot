package com.iteaj.iot.codec.adapter;

import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.codec.IotMessageDecoder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;

public class DelimiterBasedFrameMessageDecoderAdapter extends DelimiterBasedFrameDecoder implements IotMessageDecoder {

    private Class<? extends SocketMessage> messageClass;

    public DelimiterBasedFrameMessageDecoderAdapter(int maxFrameLength, ByteBuf delimiter) {
        super(maxFrameLength, delimiter);
    }

    public DelimiterBasedFrameMessageDecoderAdapter(int maxFrameLength, boolean stripDelimiter, ByteBuf delimiter) {
        super(maxFrameLength, stripDelimiter, delimiter);
    }

    public DelimiterBasedFrameMessageDecoderAdapter(int maxFrameLength, boolean stripDelimiter, boolean failFast, ByteBuf delimiter) {
        super(maxFrameLength, stripDelimiter, failFast, delimiter);
    }

    public DelimiterBasedFrameMessageDecoderAdapter(int maxFrameLength, ByteBuf... delimiters) {
        super(maxFrameLength, delimiters);
    }

    public DelimiterBasedFrameMessageDecoderAdapter(int maxFrameLength, boolean stripDelimiter, ByteBuf... delimiters) {
        super(maxFrameLength, stripDelimiter, delimiters);
    }

    public DelimiterBasedFrameMessageDecoderAdapter(int maxFrameLength, boolean stripDelimiter, boolean failFast, ByteBuf... delimiters) {
        super(maxFrameLength, stripDelimiter, failFast, delimiters);
    }

    @Override
    protected SocketMessage decode(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception {
        return this.proxy(ctx, (ByteBuf) super.decode(ctx, buffer));
    }

    @Override
    public Class<? extends SocketMessage> getMessageClass() {
        return this.messageClass;
    }

    @Override
    public IotMessageDecoder setMessageClass(Class<? extends SocketMessage> messageClass) {
        this.messageClass = messageClass;
        return this;
    }
}
