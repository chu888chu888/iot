package com.iteaj.iot.codec.adapter;

import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.codec.IotMessageDecoder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.FixedLengthFrameDecoder;

/**
 * create time: 2021/8/13
 *  固定字段解码适配器
 * @author iteaj
 * @since 1.0
 */
public class FixedLengthFrameDecoderAdapter extends FixedLengthFrameDecoder implements IotMessageDecoder {

    private Class<?> messageClass;

    /**
     * Creates a new instance.
     *
     * @param frameLength the length of the frame
     */
    public FixedLengthFrameDecoderAdapter(int frameLength) {
        super(frameLength);
    }


    @Override
    protected SocketMessage decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        return this.proxy(ctx, (ByteBuf) super.decode(ctx, in));

    }

    @Override
    public Class<? extends SocketMessage> getMessageClass() {
        return (Class<? extends SocketMessage>) this.messageClass;
    }

    @Override
    public IotMessageDecoder setMessageClass(Class<? extends SocketMessage> messageClass) {
        this.messageClass = messageClass;
        return this;
    }
}
