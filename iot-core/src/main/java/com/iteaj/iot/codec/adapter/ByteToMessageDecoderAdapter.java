package com.iteaj.iot.codec.adapter;

import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.codec.IotMessageDecoder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public abstract class ByteToMessageDecoderAdapter extends ByteToMessageDecoder implements IotMessageDecoder {

    private Class<? extends SocketMessage> messageClass;

    public ByteToMessageDecoderAdapter(Class<? extends SocketMessage> messageClass) {
        this.messageClass = messageClass;
    }

    @Override
    protected final void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        SocketMessage message = proxy(ctx, in);
        if (message != null) {
            out.add(message);
        }
    }

    @Override
    public abstract SocketMessage doDecode(ChannelHandlerContext ctx, ByteBuf in);

    @Override
    public Class<? extends SocketMessage> getMessageClass() {
        return this.messageClass;
    }

    @Override
    public IotMessageDecoder setMessageClass(Class<? extends SocketMessage> messageClass) {
        this.messageClass = messageClass;
        return this;
    }
}
