package com.iteaj.iot.codec.adapter;

import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.codec.IotMessageDecoder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import java.nio.ByteOrder;

public class LengthFieldBasedFrameMessageDecoderAdapter extends LengthFieldBasedFrameDecoder implements IotMessageDecoder {

    private Class<? extends SocketMessage> messageClass;

    public LengthFieldBasedFrameMessageDecoderAdapter(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength);
    }

    public LengthFieldBasedFrameMessageDecoderAdapter(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip);
    }

    public LengthFieldBasedFrameMessageDecoderAdapter(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip, boolean failFast) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip, failFast);
    }

    public LengthFieldBasedFrameMessageDecoderAdapter(ByteOrder byteOrder, int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip, boolean failFast) {
        super(byteOrder, maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip, failFast);
    }

    @Override
    public Class<? extends SocketMessage> getMessageClass() {
        return this.messageClass;
    }

    @Override
    public IotMessageDecoder setMessageClass(Class<? extends SocketMessage> messageClass) {
        this.messageClass = messageClass;
        return this;
    }

    @Override
    public SocketMessage decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        return this.proxy(ctx, (ByteBuf) super.decode(ctx, in));
    }
}
