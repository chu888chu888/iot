package com.iteaj.iot;

/**
 * <p>协议对应的业务处理器</p>
 * Create Date By 2020-09-21
 * @author iteaj
 * @since 1.8
 */
public interface ProtocolHandle<T extends Protocol> {

    /**
     * 协议的业务处理
     * @param protocol
     */
    Object handle(T protocol);

}
