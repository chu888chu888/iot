package com.iteaj.iot.plc.omron;

import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.client.ClientComponent;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.codec.adapter.LengthFieldBasedFrameMessageDecoderAdapter;
import com.iteaj.iot.plc.PlcTcpClient;
import com.iteaj.iot.utils.ByteUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;

import java.nio.ByteOrder;

/**
 * 欧姆龙PLC基于tcp实现的客户端
 */
public class OmronTcpClient extends PlcTcpClient {

    // 握手信号报文
    private final byte[] handSingleMessage = new byte[] {
        0x46, 0x49, 0x4E, 0x53, // FINS
        0x00, 0x00, 0x00, 0x0C, // 后面的命令长度
        0x00, 0x00, 0x00, 0x00, // 命令码
        0x00, 0x00, 0x00, 0x00, // 错误码
        0x00, 0x00, 0x00, 0x01  // 节点号
    };

    public OmronTcpClient(ClientComponent clientComponent, OmronConnectProperties config) {
        super(clientComponent, config);
        this.handSingleMessage[19] = config.getSA1();
    }

    @Override
    protected ChannelInboundHandler createProtocolDecoder() {
        return new LengthFieldBasedFrameMessageDecoderAdapter(ByteOrder.BIG_ENDIAN, 1024
                , 4, 4, 0, 0, true) {

            @Override
            public SocketMessage doDecode(ChannelHandlerContext ctx, ByteBuf decode) {
                int readableBytes = (decode).readableBytes();
                byte[] message = new byte[readableBytes];
                (decode).readBytes(message);

                if(message.length != 24) {
                    String channelId = ctx.channel().id().asShortText();
                    return new OmronMessage(message).setChannelId(channelId);
                } else {
                    if(logger.isDebugEnabled()) {
                        logger.debug("PLC({}) 握手响应 - 状态：{} - 报文：{}"
                                , getName(), "成功", ByteUtil.bytesToHex(message));
                    }
                }

                return null;
            }
        };
    }

    @Override
    public void successCallback(ChannelFuture future) {
        // 发送PLC的握手报文
        future.channel().writeAndFlush(Unpooled.wrappedBuffer(handSingleMessage)).addListener(call -> {
            if(call.isSuccess()) {
                if(logger.isDebugEnabled()) {
                    logger.debug("PLC({}) 握手请求 - 状态：{} - 报文：{}"
                            , getName(), "成功", ByteUtil.bytesToHex(handSingleMessage));
                }
            } else {
                logger.error("PLC({}) 握手请求 - 状态：{} - 报文：{}"
                        , getName(), "失败", ByteUtil.bytesToHex(handSingleMessage));
            }
        });
    }

    @Override
    public OmronConnectProperties getConfig() {
        return (OmronConnectProperties) super.getConfig();
    }
}
