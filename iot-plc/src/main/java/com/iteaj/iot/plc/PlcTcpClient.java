package com.iteaj.iot.plc;

import com.iteaj.iot.client.ClientComponent;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.config.ConnectProperties;

public abstract class PlcTcpClient extends TcpSocketClient {

    public PlcTcpClient(ClientComponent clientComponent, ClientConnectProperties config) {
        super(clientComponent, config);
    }
}
