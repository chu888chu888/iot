package com.iteaj.iot.plc.siemens;

import com.iteaj.iot.client.ClientConnectProperties;

public class SiemensConnectProperties extends ClientConnectProperties {

    /**
     * 槽号
     */
    private byte slot;

    /**
     * 机架号
     */
    private byte rack;

    /**
     * 西门子型号
     */
    private SiemensModel model;

    public SiemensConnectProperties(String host) {
        this(host, SiemensModel.S1200);
    }

    public SiemensConnectProperties(String host, byte slot, byte rack) {
        this(host, 102, SiemensModel.S1200, slot, rack);
    }

    public SiemensConnectProperties(String host, SiemensModel model) {
        this(host, 102, model);
    }

    public SiemensConnectProperties(String host, int port, SiemensModel model) {
        this(host, port, model, (byte) 0, (byte) 0);
    }

    public SiemensConnectProperties(String host, Integer port, SiemensModel model, String connectKey) {
        super(host, port, connectKey);
        this.model = model;
    }

    public SiemensConnectProperties(String host, int port, SiemensModel model, byte slot, byte rack) {
        super(host, port, host + ":" + port + ":" + model + ":" + rack + "_" + slot);
        this.slot = slot;
        this.rack = rack;
        this.model = model;
    }

    public SiemensModel getModel() {
        return model;
    }

    public void setModel(SiemensModel model) {
        this.model = model;
    }

    public byte getSlot() {
        return slot;
    }

    public void setSlot(byte slot) {
        this.slot = slot;
    }

    public byte getRack() {
        return rack;
    }

    public void setRack(byte rack) {
        this.rack = rack;
    }
}
