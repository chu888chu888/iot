package com.iteaj.iot.plc;

import com.iteaj.iot.ProtocolException;

public class PlcException extends ProtocolException {

    public PlcException(Object protocolType) {
        super(protocolType);
    }

    public PlcException(String message) {
        super(message);
    }

    public PlcException(String message, Object protocolType) {
        super(message, protocolType);
    }

    public PlcException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlcException(Throwable cause) {
        super(cause);
    }

    public PlcException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
