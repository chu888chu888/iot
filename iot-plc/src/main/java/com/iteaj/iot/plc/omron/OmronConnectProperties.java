package com.iteaj.iot.plc.omron;

import com.iteaj.iot.client.ClientConnectProperties;
import org.springframework.util.StringUtils;

public class OmronConnectProperties extends ClientConnectProperties {

    /**
     * 上位机的节点地址，如果你的电脑的Ip地址为192.168.0.8，那么这个值就是8
     */
    private byte SA1;

    /**
     * PLC的节点地址，这个值在配置了ip地址之后是默认赋值的，默认为Ip地址的最后一位
     */
    private byte DA1;

    protected OmronConnectProperties() { }

    public OmronConnectProperties(String host, Integer port) {
        this(host, port, host+":"+port);
    }

    /**
     *
     * @param host
     * @param port
     * @param connectKey 用来声明客户端的唯一性
     */
    public OmronConnectProperties(String host, Integer port, String connectKey) {
        super(host, port, connectKey);
    }

    public OmronConnectProperties(String host, Integer port, long allIdleTime, long readerIdleTime, long writerIdleTime) {
        super(host, port, allIdleTime, readerIdleTime, writerIdleTime);
        this.setHost(host);
    }

    public byte getSA1() {
        return SA1;
    }

    public void setSA1(byte SA1) {
        this.SA1 = SA1;
    }

    public byte getDA1() {
        return DA1;
    }

    public void setDA1(byte DA1) {
        this.DA1 = DA1;
    }

    /**
     * 请使用ip地址
     * @param ipAddress
     */
    @Override
    public void setHost(String ipAddress) {
        super.setHost(ipAddress);
        if(StringUtils.hasText(ipAddress)) {
            DA1 = (byte) Integer.parseInt(ipAddress.substring(ipAddress.lastIndexOf( "." ) + 1));
        }
    }
}
