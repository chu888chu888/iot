package com.iteaj.iot.client.proxy;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.ClientProperties;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.component.TcpClientComponent;
import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.proxy.ProxyClientMessage;

public class ProxyClientComponent extends TcpClientComponent<ProxyClientMessage> {

    public ProxyClientComponent(ClientProperties.ClientProxyConnectProperties config) {
        super(config);
    }

    @Override
    public TcpSocketClient createNewClient(ClientConnectProperties config) {
        ClientProperties.ClientProxyConnectProperties clientProxy;
        if(config instanceof ClientProperties.ClientProxyConnectProperties) {
            clientProxy = (ClientProperties.ClientProxyConnectProperties) config;
        } else {
            throw new IllegalArgumentException("请使用代理客户端配置对象[ClientProperties.ClientProxy]");
        }

        return new ProxyClient(this, clientProxy);
    }

    @Override
    public String getName() {
        return "代理客户端";
    }

    @Override
    public String getDesc() {
        return "用于客户端对设备的代理操作";
    }

    @Override
    public AbstractProtocol getProtocol(ProxyClientMessage message) {
        String messageId = message.getMessageId();

        return remove(messageId);
    }
}
