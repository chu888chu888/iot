package com.iteaj.iot.client.protocol;

import com.iteaj.iot.ProtocolException;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.*;
import com.iteaj.iot.client.component.SingleTcpClientComponent;
import com.iteaj.iot.client.component.SocketClientComponent;
import com.iteaj.iot.protocol.socket.AbstractSocketProtocol;
import io.netty.channel.socket.DatagramPacket;
import org.springframework.core.GenericTypeResolver;

/**
 * create time: 2021/8/6
 *
 * @author iteaj
 * @since 1.0
 */
public abstract class ClientSocketProtocol<C extends ClientMessage> extends AbstractSocketProtocol<C> implements ClientProtocol<C> {

    /**
     * udp协议报文
     */
    private DatagramPacket packet;

    /**
     * 用来获取客户端
     * @see SocketClient
     * @see #getIotClient()
     */
    private ClientConnectProperties clientKey;

    private ClientProtocolHandle defaultProtocolHandle; // 默认协议处理器

    @Override
    public C requestMessage() {
        return super.requestMessage();
    }

    @Override
    public C responseMessage() {
        return super.responseMessage();
    }

    @Override
    public abstract ProtocolType protocolType();

    /**
     * 返回客户端
     * @return
     */
    public SocketClient getIotClient() {
        ClientComponent clientComponent = IotClientBootstrap.getClientComponentFactory().getByClass(this.getMessageClass());
        if(clientComponent instanceof SingleTcpClientComponent && getClientKey() != null) {
            throw new ProtocolException("组件["+clientComponent.getName()+"]属于单客户端组件, 不支持使用方法 #request(host, port)");
        }

        Object clientKey = this.getClientKey() != null ? this.getClientKey() : clientComponent.getConfig();
        IotClient socketClient = clientComponent.getClient(clientKey);
        if(socketClient instanceof SocketClient) {
            return (SocketClient) socketClient;
        } else {
            throw new ProtocolException("客户端["+clientKey+"]不存在");
        }
    }

    protected Class<C> getMessageClass() {
        if(requestMessage() != null) {
            return (Class<C>) requestMessage().getClass();
        } else {
            return (Class<C>) GenericTypeResolver.resolveTypeArgument(getClass(), ClientSocketProtocol.class);
        }
    }

    protected String getMessageId() {
        ClientMessage message = requestMessage();
        return message.getMessageId();
    }

    public ClientProtocolHandle getDefaultProtocolHandle() {
        if(defaultProtocolHandle == null) {
            defaultProtocolHandle = (ClientProtocolHandle) IotClientBootstrap
                    .businessFactory.getProtocolHandle(getClass());
        }

        return defaultProtocolHandle;
    }

    public DatagramPacket getPacket() {
        return packet;
    }

    public void setPacket(DatagramPacket packet) {
        this.packet = packet;
    }

    public ClientConnectProperties getClientKey() {
        return clientKey;
    }

    public ClientSocketProtocol setClientKey(ClientConnectProperties clientKey) {
        this.clientKey = clientKey;
        return this;
    }
}
