package com.iteaj.iot.client.component;

import com.iteaj.iot.ConcurrentStorageManager;
import com.iteaj.iot.client.ClientComponent;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.IotClient;
import com.iteaj.iot.client.MultiClientManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 默认的客户端管理实现
 */
public class SimpleMultiClientManager extends ConcurrentStorageManager<String, IotClient> implements MultiClientManager {

    private ClientComponent clientComponent;

    public SimpleMultiClientManager(ClientComponent clientComponent) {
        this.clientComponent = clientComponent;
    }

    @Override
    public synchronized void addClient(Object clientKey, IotClient value) {
        String clientKeyStr;
        if(clientKey instanceof ClientConnectProperties) {
            clientKeyStr = ((ClientConnectProperties) clientKey).connectKey();
        } else {
            clientKeyStr = clientKey.toString();
        }

        if(!isExists(clientKeyStr)) {
            add(clientKeyStr, value);
        }
    }

    @Override
    public IotClient getClient(Object clientKey) {
        String clientKeyStr;
        if(clientKey instanceof ClientConnectProperties) {
            clientKeyStr = ((ClientConnectProperties) clientKey).connectKey();
        } else {
            clientKeyStr = clientKey.toString();
        }

        return get(clientKeyStr);
    }

    @Override
    public IotClient removeClient(Object clientKey) {
        String clientKeyStr;
        if(clientKey instanceof ClientConnectProperties) {
            clientKeyStr = ((ClientConnectProperties) clientKey).connectKey();
        } else {
            clientKeyStr = clientKey.toString();
        }

        return remove(clientKeyStr);
    }

    @Override
    public List<IotClient> clients() {
        return new ArrayList<>(getMapper().values());
    }

    @Override
    public ClientComponent getClientComponent() {
        return this.clientComponent;
    }

    @Override
    public void setClientComponent(ClientComponent clientComponent) {
        this.clientComponent = clientComponent;
    }

}
