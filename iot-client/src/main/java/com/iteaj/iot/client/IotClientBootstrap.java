package com.iteaj.iot.client;

import com.iteaj.iot.*;
import com.iteaj.iot.business.BusinessFactory;
import com.iteaj.iot.client.component.SingleTcpClientComponent;
import com.iteaj.iot.client.component.SocketClientComponent;
import com.iteaj.iot.client.http.HttpManager;
import com.iteaj.iot.client.http.okhttp.OkHttpManager;
import com.iteaj.iot.client.proxy.ProxyClientComponent;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@EnableConfigurationProperties(ClientProperties.class)
public class IotClientBootstrap implements InitializingBean, ApplicationListener<ContextRefreshedEvent>, ApplicationContextAware {

    public static BusinessFactory businessFactory;
    public static ApplicationContext applicationContext;
    public static ClientComponentFactory clientComponentFactory;

    @Autowired
    private ClientProperties properties;
    public static HttpManager httpManager;
    public static DeviceManager deviceManager;
    protected static IotThreadManager threadManager;
//    public static ThreadPoolTaskExecutor taskExecutor;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        /**
         * 只有在将客户端和服务端在同一个应用上下文的时候才会有设备管理
         */
        if(applicationContext.containsBean("deviceManager")) {
            IotClientBootstrap.deviceManager = applicationContext.getBean(DeviceManager.class);
        }

        // 连接组件默认的客户端
        clientComponentFactory.getComponents().forEach(item -> item.connect());

        // 连接完成回调
        clientComponentFactory.getComponents().forEach(item -> item.finished());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        httpManager = applicationContext.getBean(HttpManager.class);
        threadManager = applicationContext.getBean(IotThreadManager.class);
        clientComponentFactory = applicationContext.getBean(ClientComponentFactory.class);

        // 初始化所有客户端
        if(!CollectionUtils.isEmpty(clientComponentFactory.getComponents())) {
            clientComponentFactory.getComponents().forEach(item -> {
                if(item instanceof SocketClientComponent) {
                    item.init(threadManager.getWorkerGroup());
                } else if(item instanceof SingleTcpClientComponent) {
                    item.init(threadManager.getWorkerGroup());
                } else {
                    item.init();
                }
            });
        }
    }

    public static <T extends IotClient> T getClient(Class<? extends ClientMessage> clazz, Object clientKey) {
        ClientComponent clientComponent = clientComponentFactory.getByClass(clazz);

        /**
         * 通过报文类型{@link ClientMessage}获取唯一对应的组件{@link ClientComponent#getMessageClass()}
         * 且必须注册到spring容器
         */
        if(clientComponent == null) {
            throw new IllegalArgumentException("未注册与报文类型[" + clazz.getSimpleName()
                    +"]对应的客户端组件[" + ClientComponent.class.getSimpleName() + "]" );
        }

        if(null == clientKey) {
            // 使用默认的客户端
            return (T) clientComponent.getClient();
        } else {
            // 使用指定的客户端
            return (T) clientComponent.getClient(clientKey);
        }
    }

    public static <T> T getBean(Class<T> requiredClass) {
        return applicationContext.getBean(requiredClass);
    }

    @Bean("httpManager")
    @ConditionalOnMissingBean(HttpManager.class)
    public HttpManager httpManager() {
        return new OkHttpManager();
    }

    /**
     * 返回客户端组件工厂
     * @return
     */
    public static ClientComponentFactory getClientComponentFactory() {
        return clientComponentFactory;
    }

    public static IotProtocolFactory getProtocolFactory(Class<? extends ClientMessage> clazz) {
        return clientComponentFactory.getByClass(clazz).protocolFactory();
    }

    /**
     * 发布应用事件
     * @param event
     */
    public static void publishApplicationEvent(ApplicationEvent event) {
        applicationContext.publishEvent(event);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        IotClientBootstrap.applicationContext = applicationContext;
    }

    @Bean
    public ClientProtocolTimeoutManager clientProtocolTimeoutManager(ClientComponentFactory factory) {
        List<ProtocolTimeoutStorage> timeoutStorages = factory.getComponents().stream()
                .filter(item -> item.protocolFactory() instanceof ProtocolFactory)
                .map(item -> ((ProtocolFactory<?>) item.protocolFactory()).getDelegation())
                .collect(Collectors.toList());

        return new ClientProtocolTimeoutManager(timeoutStorages);
    }

    @Bean
    public ClientProtocolHandleFactory clientBusinessFactory() {
        IotClientBootstrap.businessFactory = new ClientProtocolHandleFactory();
        return (ClientProtocolHandleFactory) IotClientBootstrap.businessFactory;
    }

    @Bean
    public ClientComponentFactory clientComponentFactory() {
        return new ClientComponentFactory();
    }

    @Bean
    @ConditionalOnExpression("!${iot.client.proxy.port:'false'}.equals('false')")
    public ProxyClientComponent proxyClientComponent() {
        return new ProxyClientComponent(properties.getProxy());
    }

}
