package com.iteaj.iot.client.component;

import com.iteaj.iot.client.*;
import com.iteaj.iot.*;
import com.iteaj.iot.config.ConnectProperties;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;

/**
 * 基于socket的客户端组件 主要tcp, upd等
 * @param <M>
 */
public abstract class SocketClientComponent<M extends ClientMessage> extends ProtocolFactory<M>
        implements ClientComponent<M>, IotProtocolFactory<M>, InitializingBean {

    private IotClient iotClient;
    private Class<M> messageClass;
    private IotThreadManager threadManager;
    private ClientConnectProperties config;
    private MultiClientManager clientManager;
    public SocketClientComponent(ClientConnectProperties config) {
        this(config, new SimpleMultiClientManager(null));
    }

    public SocketClientComponent(ClientConnectProperties config, MultiClientManager clientManager) {
        this.config = config;
        this.clientManager = clientManager;
        this.setDelegation(createProtocolTimeoutStorage());

        if(this.clientManager.getClientComponent() == null) {
            this.clientManager.setClientComponent(this);
        }
    }

    @Override
    public void init(Object ...args) {
        this.getClient().init(args[0]);
    }

    @Override
    public void connect() {
        this.getClient().connect(null, 3000);
    }

    @Override
    public void addClient(Object clientKey, IotClient value) {
        this.clientManager.addClient(clientKey, value);
    }

    @Override
    public IotClient getClient() {
        if(this.iotClient != null) {
            return this.iotClient;
        } else {
            if(this.getConfig() != null) {
                return this.iotClient = createNewClient(this.config);
            }
        }

        return null;
    }

    @Override
    public List<IotClient> clients() {
        return clientManager.clients();
    }

    @Override
    public IotClient getClient(Object clientKey) {
        return clientManager.getClient(clientKey);
    }

    @Override
    public IotClient removeClient(Object clientKey) {
        return clientManager.removeClient(clientKey);
    }

    @Override
    public void setClientComponent(ClientComponent component) {
        this.clientManager.setClientComponent(component);
    }

    @Override
    public Class<M> getMessageClass() {
        if(this.messageClass == null) {
            // @since 2.3.0 只解析一次报文类
            this.messageClass = ClientComponent.super.getMessageClass();
        }

        return this.messageClass;
    }

    /**
     * 创建一个新客户端
     * @param config
     * @return
     */
    public abstract SocketClient createNewClient(ClientConnectProperties config);

    /**
     * 创建新的客户端并连接
     * @param config
     */
    public SocketClient createNewClientAndConnect(ClientConnectProperties config) {
        SocketClient newClient = createNewClient(config);
        EventLoopGroup workerGroup = threadManager.getWorkerGroup();
        newClient.init((NioEventLoopGroup) workerGroup);
        newClient.connect(null, 3000);
        return newClient;
    }

    @Override
    public IotProtocolFactory protocolFactory() {
        return this;
    }

    public MultiClientManager getClientManager() {
        return clientManager;
    }

    protected ProtocolTimeoutStorage createProtocolTimeoutStorage() {
        return new ProtocolTimeoutStorage(getName());
    }

    public ClientConnectProperties getConfig() {
        return config;
    }

    public void setConfig(ClientConnectProperties config) {
        this.config = config;
    }

    @Autowired
    protected void setThreadManager(IotThreadManager threadManager) {
        this.threadManager = threadManager;
    }

    @Override
    public void afterPropertiesSet() throws Exception { }
}
