package com.iteaj.iot.client.codec;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.TcpSocketClient;
import com.iteaj.iot.client.ClientComponent;
import com.iteaj.iot.codec.adapter.ByteToMessageDecoderAdapter;
import com.iteaj.iot.config.ConnectProperties;

public abstract class ByteToMessageDecoderClient extends TcpSocketClient {

    public ByteToMessageDecoderClient(ClientComponent clientComponent, ClientConnectProperties config) {
        super(clientComponent, config);
    }

    @Override
    protected abstract ByteToMessageDecoderAdapter createProtocolDecoder();
}
