package com.iteaj.iot.client;

import com.iteaj.iot.codec.IotMessageDecoder;
import com.iteaj.iot.config.ConnectProperties;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * create time: 2021/8/6
 *
 * @author iteaj
 * @since 1.0
 */
public abstract class TcpSocketClient extends SocketClient {

    public TcpSocketClient(ClientComponent clientComponent, ClientConnectProperties config) {
        super(clientComponent, config);
    }

    protected Class<? extends Channel> channel() {
        return NioSocketChannel.class;
    }

    /**
     * @see IotMessageDecoder 基于netty的常用tcp解码器适配
     * @return
     */
    @Override
    protected abstract ChannelInboundHandler createProtocolDecoder();
}
