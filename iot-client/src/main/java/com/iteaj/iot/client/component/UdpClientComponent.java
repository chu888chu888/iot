package com.iteaj.iot.client.component;

import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.UdpSocketClient;
import com.iteaj.iot.client.ClientMessage;
import com.iteaj.iot.client.MultiClientManager;
import com.iteaj.iot.config.ConnectProperties;

/**
 * 基于udp实现的客户端
 */
public abstract class UdpClientComponent<M extends ClientMessage> extends SocketClientComponent<M> {

    public UdpClientComponent(ClientConnectProperties config) {
        super(config);
    }

    public UdpClientComponent(ClientConnectProperties config, MultiClientManager clientManager) {
        super(config, clientManager);
    }

    @Override
    public void init(Object ...args) {
        this.getClient().init(args[0]);
    }

    public abstract UdpSocketClient createNewClient(ClientConnectProperties config);

}
