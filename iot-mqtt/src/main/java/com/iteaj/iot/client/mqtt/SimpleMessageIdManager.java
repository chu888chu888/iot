package com.iteaj.iot.client.mqtt;

import com.iteaj.iot.ConcurrentStorageManager;
import com.iteaj.iot.client.mqtt.message.MqttClientMessage;
import io.netty.handler.codec.mqtt.MqttMessageType;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class SimpleMessageIdManager extends ConcurrentStorageManager<Object, MessageMapper> implements MessageIdManager<Object>{

    private MqttClient client;
    private MqttClientComponent clientComponent;
    private AtomicInteger messageId = new AtomicInteger(1);
    private static Logger logger = LoggerFactory.getLogger(SimpleMessageIdManager.class);


    public SimpleMessageIdManager(MqttClient client, MqttClientComponent clientComponent) {
        this.client = client;
        this.clientComponent = clientComponent;
    }

    @Override
    public synchronized int nextId() {
        int andIncrement = messageId.getAndIncrement();
        if(andIncrement >= 0xffff) {
            messageId.set(1);
        }

        /**
         * 这里需要验证此PacketId是否可以使用
         */
        if(isExists(CLIENT_PREFIX+andIncrement)) {
            return nextId();
        }

        return andIncrement;
    }

    @Override
    public void expire() {
        final Iterator<Map.Entry<Object, MessageMapper>> iterator = getMapper().entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<Object, MessageMapper> entry = iterator.next();
            MessageMapper message = entry.getValue();
            final long expire = System.currentTimeMillis() - message.getLastTime();

            // 三十秒后重新发送
            if(expire > 30000) {

                // 重发50次然后校验一次是否移除此报文
                if(message.getCount() >= 50) {
                    // 服务端没有确认报文, 判断是否需要移除
                    if(clientComponent.getPublishListener().remove(client, message)) {
                        iterator.remove(); break;
                    }
                }

                // 重新发送报文
                if(client != null) {
                    message.inc(); // 递增重发次数
                    message.setLastTime(System.currentTimeMillis()); // 修改重发时间

                    final MqttClientMessage clientMessage = message.getMessage();
                    final MqttPublishMessage mqttMessage = client
                            .buildPublishDupMessage(clientMessage, message.getPacketId());

                    client.getChannel().writeAndFlush(mqttMessage).addListener(future -> {
                        final int packetId = mqttMessage.variableHeader().packetId();
                        if(future.isSuccess()) {
                            if(logger.isDebugEnabled()) {
                                logger.debug("mqtt({}) {}(重发成功) - PacketId：{} - 远程主机：{} - 重发次数：{}"
                                        , clientComponent.getName(), MqttMessageType.PUBLISH
                                        , packetId, message.getProperties(), message.getCount());
                            } else {
                                logger.warn("mqtt({}) {}(重发失败) - PacketId：{} - 远程主机：{} - 重发次数：{}"
                                        , clientComponent.getName(), MqttMessageType.PUBLISH
                                        , packetId, message.getProperties(), message.getCount(), future.cause());
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    public MessageMapper get(Object packetId) {
        return super.get(CLIENT_PREFIX + packetId);
    }

    @Override
    public MessageMapper remove(Object packetId) {
        return super.remove(CLIENT_PREFIX+packetId);
    }

    @Override
    public MessageMapper add(Object key, MessageMapper val) {
        return super.add(CLIENT_PREFIX+key, val);
    }

    @Override
    public MessageMapper getServer(Integer packetId) {
        return super.get(SERVER_PREFIX+packetId);
    }

    @Override
    public MessageMapper addServer(Integer packetId, MessageMapper mapper) {
        return super.add(SERVER_PREFIX+packetId, mapper);
    }

    @Override
    public MessageMapper removeServer(Integer packetId) {
        return super.remove(SERVER_PREFIX+packetId);
    }
}
